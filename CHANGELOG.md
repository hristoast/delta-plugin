# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Added name of plugin to warning when records are skipped due to missing NAME subrecord.

## [0.17.6] - 2023-05-14
### Fixed
- Reverts change from 0.17.4:
  >  Records modified by fewer than three plugins will be skipped and an error will be displayed.
  >  This should only occur when there is a bug in DeltaPlugin.
  This was determined to cause false positives when a third plugin which includes a copy of its
  masters' version of the record causes the record to change to skip the duplicated data
  (e.g. Plugin A is the original, Plugin B modifies the record, then Plugin C includes Plugin
  A's record without changes).
- Fixed bug in the parsing of the `--openmw-cfg` argument.

## [0.17.5] - 2023-04-29

### Fixed
- Fixed deletions of exterior cell names being ignored, again (#41).

## [0.17.4] - 2023-04-26

### Changed
- Records modified by fewer than three plugins will be skipped and an error will be displayed.
  This should only occur when there is a bug in DeltaPlugin.

### Fixed
- Fixed deletions of exterior cell names being ignored (#41).

## [0.17.3] - 2023-04-13
### Fixed
- Fixed panic when passing a value to the `out_dir` argument of the `merge` subcommand.

## [0.17.2] - 2023-04-12

### Dependencies
- Minimum rust version for compiling is now 1.60

### Added
- Updated to openmw-cfg 0.3 adding support for specifying the openmw config file using OPENMW_CONFIG_DIR,
  a PATH-like variable which can contain a list of directories to look for openmw.cfg (used by portmod),
  as well as support for tilde expansion,and a better default if neither OPENMW_CONFIG nor OPENMW_CONFIG_DIR   are set.

### Changed
- Invalid flags in plugins will now be ignored instead of causing plugins to fail to load.

### Fixed
- Fixed off-by-one error when detecting if cell master indices are valid.

## [0.17.1] - 2022-12-23
### Fixes
- Fixed serialization of Ingredient records where the merge resulted in more than 4 effects. This
  can occur if a multiple plugins replace the same effect with something different. Note that the
  effect list will be truncated and may not be as expected, but it won't cause a malformed record
  anymore.

## [0.17.0] - 2022-12-07

### Changed
- Faction ranks are now identified by their index rather than by name (breaks conversion
  format and diff format). This means that when merging, mods adding new ranks to the same
  faction will override each others new ranks, rather than each adding a separate new rank for each.

### Fixes
- Bug where factions may end up with more than 10 rank names, as the ranks data is truncated at 10,
  but the rank names weren't.
- Teleport destinations will no longer be garbled if a change within the original cell overrides a
  change to a different cell and position. Note that these two things can't be merged cleanly and
  could indicate incompatible mods.

## [0.16.1] - 2022-08-21

### Dependencies
- Minimum Rust version is 1.57 (also applies to 0.16.0)

### Fixes
- Fixed self-dependency in the merged plugin when a non-default plugin name is used and
  the plugin already exists in the load order.

## [0.16.0] - 2022-08-15

### Changed
- Running `delta_plugin` without arguments now produces a help message.

### Added
- Size subcommand which prints the number of records in a plugin (for use by configtool).

### Fixes
- Fixed version parsing bug when building outside the git repository (#23)
- Fixed deserializing empty record identifiers from yaml.
- Fixed record count in esm plugin headers to include DialogueInfo records
- Improved error message when `delta_plugin deps` is passed a file which doesn't exist.

## [0.15.0] - 2022-02-04

### Changed
- Various error messages have been added or improved
- Namespaced all records by type, preventing identifier conflicts between different types of records.
  This breaks compatibility with older versions of the text format (#21).

## [0.14.1] - 2022-01-08

### Fixes
- Missing unknown Cell trait flag 32.

## [0.14.0] - 2021-11-04

### Added
- Plugin loading errors will no longer cause the merging process to fail. Instead, the plugin causing
  the error will be excluded from the merged plugin and an error message will be displayed.
- `.omwscripts` files will be ignored, as they do not need to be merged (note: as these have not been included
  in an OpenMW release, it's possible they will change between now and when they are released, so support may
  need to change in a future release).
- Unrecognised plugin types will now produce warnings

### Changed
- `delta_plugin convert` now does a regular conversion (without comparing the plugin to its masters) by default,
  as if the `--no-compare` argument was passed. The `--no-conmpare` argument has been replaced with a `--compare`
  argument.

## [0.13.1] - 2021-09-07

### Fixes
- Invalid Merged plugin created when Landscape records are merged
  (landscape records will no longer be treated as mergeable, as it would be very complex to
  track LandTexture palette sources so that an appropriate collection of LandTextures can be
  included in the merged plugin)
- Build error on rust 1.49-1.52 (1.49 is now being tracked as the minimum supported rust version).

## [0.13.0] - 2021-08-09

### Added
- `deps` subcommand which displays binary plugin dependencies

## [0.12.1] - 2021-07-16

### Bugfixes
- Fixed missing error descriptions
- Switched to using the [similar](https://github.com/mitsuhiko/similar) library for `delta_plugin diff`
  as its output is less buggy than diffy (which is still used to apply patches)
- Removed unnecessary newline from output of `delta_plugin diff` when plugins are identical
- Invalid records will no longer cause the program to abort. Instead, they will be skipped and
  an error message will be displayed.

## [0.12.0] - 2021-07-01

### Added
- `-n/--no-compare` option for the `convert` subcommand. This produces a yaml plugin without
  delta records, containing all the information from the original esp.
- `diff` and `apply` subcommands, for creating/applying unified diff patches directly from/onto
  esp files. These compare the plugins as if they were converted using `convert --inline --no-compare`,
  with the additional change of removing the `__esp_version__`, `__plugin_type__` and
  `transpiler_version` header fields to avoid conflicts due to patch context (also noting that
  deltaplugin always outputs esp version 1.3).

### Changed
- The `version` and `masters.version` fields are now optional, instead of defaulting to `0.0.0`.

### Bugfixes
- Faction records are now still valid if there are more than 2 attributes, or more than 7 skills
  extra attributes/skills will be truncated when converting to esp.
- The existence of the GIT_VERSION environment variable (added automatically if a git
  repository is detected) is no longer mandatory during builds.
- Fixed handling of blood types in Creature and NPC records. Previously they were handled as
  traits, however this was only accounting for the three blood types included in the vanilla
  game. Now they are stored as a separate blood index (defaulting to 0, for normal blood),
  which corresponds to one of various blood textures defined as fallback values with the key
  `Blood_Texture_<index>`.
  Note that this change breaks text-format compatibility for Creatures/NPCs which use
  non-default blood.

## [0.11.1] - 2021-06-12

### Bugfixes
- Fixed regression since 0.11.0 causing DialogueInfo records to be deleted unnecessarily
- Fixed extra informational messages about merged records for records which aren't actually
  being included in the merged plugin.
- Fixed deserialization of nested records (DialogueInfo and CellRef) from yaml so that
  deltas and non-deltas are always distinguishable.

## [0.11.0] - 2021-06-12

### Added
- Re-added support for Skill and PathGrid records

### Changed
- DialgueInfo records no longer have a deleted variant in the text format. Instead they should
  be deleted by setting the `deleted` field. This was done to work around an engine requirement,
  as deleted INFO records are required to include the `prev` and `next` fields.

### Bugfixes
- Fixed building outside of git repository
- Fixed serialization of deleted DialogueInfo records

## [0.10.2] - 2021-05-26

### Bugfixes
- Fixed invalid Info records when they are deleted.
- Fixed the default path of openmw.cfg on macOS.

## [0.10.1] - 2021-05-22

### Bugfixes
- Removed the unnecessary creation of a cache directory if `--debug-incremental` is not used.

## [0.10.0] - 2021-05-16

### Added
- Re-added support for Race, Region, Repair, Sound, SoundGenerator, Weapon, Spell and Startup records

### Bugfixes
- Made Class records no longer serialize omitted fields to yaml.
- Standardized cell reference master names as lowercase (should also now be case insensitive)
- Fixed case insensitivity of plugin masters

## [0.9.0] - 2021-05-06

### Added
- Re-added support for Potion and Probe records

### Bugfixes
- Added some missing unknown flags to the possible CreatureTraits

## [0.8.2] - 2021-04-28

### Bugfixes
- Fixed a bug introduced when compiling outside the context of the git repository by
  falling back to the cargo version if the git version isn't available at compile time.
- Made the model and race BodyPart fields optional (to match openmw).

## [0.8.1] - 2021-04-24

### Bugfixes
- Fixed parsing of backslashes in windows paths in openmw.cfg (was treating them as an escape character).

## [0.8.0] - 2021-04-16

### Added
- Re-added support for Landscape, Light, Lockpick, MagicEffect and NPC records

## [0.7.0] - 2021-04-12

### Added
- Re-added support for Dialogue, Door, EnchantmentEffect, Faction, Gamesetting, Global, Ingredient, LandTexture and Static records
- Added more context to some top-level error messages

### Bugfixes
- Fixed invalid field name causing an error when deserializing Apparatus records from yaml.
- Fixed `--openmw-cfg` argument, which did not accept an argument.

## [0.6.0] - 2021-03-26

### Added
- Readded support for Creature, Misc, Container and Script records

## [0.5.2] - 2021-03-19

### Bugfixes

- Made optional fields behave correctly. On Cell records, missing optional fields on records loaded later should not be considered deleted (as new cell records are loaded on top of old ones) but they should on other records.
- Made Cell names which are empty strings be treated as equivalent to not providing the cell name (OpenMW treats the situations identically, and there's no reason to patch records to fix "missing" empty cell names).

## [0.5.1] - 2021-03-17

Fixed OSX builds

## [0.5.0] - 2021-03-17

### Added
- Readded suport for Clothing records
- Added an `--openmw-cfg` command line option for setting the `openmw.cfg` path.
- Added `--skip-cells` option to skip merging of cell records. Noting that handling cells significantly
  increases memory usage.

### Changed
- Clarified error message for missing subrecords.
- `--incremental` option was renamed to `--debug-incremental` to make it clear that it's a debugging tool,
  not a feature.
- Deleted CellRefs now transform the object into a special dummy record, and activator with id `<deleted>`.
  OpenMW requires that the NAME be provided, and will produce warnings if it is an empty string.

## [0.4.0] - 2021-03-08

### Added
- Readded suport for Levelled List records

## [0.3.0] - 2021-03-03

### Added
- Readded suport for Apparatus, Armour, BirthSign, BodyPart and Book records

## [0.2.1] - 2021-02-23

### Changed
- OMWLLF.omwaddon will no longer be used to create merged plugins (to prevent a circular dependency).
- Morrowind/Tribunal/Bloodmoon will no longer be merged when creating a merged plugin.
  This doesn't appear to have any effect on the currently merged record types, however there are some
  which are known to cause discrepencies.

## [0.2.0] - 2021-02-21

Stable(ish) implementation following experimental version 0.1.0.
Currently only supports Activator and Cell records.

Creating merged plugins is stable(ish), but the yaml plugin format will be
subject to change, without support being given for upgrades (beyond
converting back to esp with the old version, and into yaml with the new version).
