# DeltaPlugin

[![docs](https://img.shields.io/badge/docs-master-blue)](https://bmwinger.gitlab.io/delta-plugin/delta_plugin/index.html)
[![minimum rustc 1.60](https://img.shields.io/badge/rustc-1.60+-blue.svg)](https://rust-lang.github.io/rfcs/2495-min-rust-version.html)

DeltaPlugin is a multi-purpose OMWAddon/ESP tool. It's primary features include a yaml transcoder, minimal diffing functionality, and using the minimal diffing functionality to produce merged plugins.

<details><summary>Usage</summary>

```
delta_plugin 0.17.6
Benjamin Winger <bmw@disroot.org>
Transcoder for managing a form of markup-based minimal Elder Scrolls Plugin files and OMWAddon files

USAGE:
    delta_plugin [FLAGS] [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -q, --quiet      Run in quiet mode
    -V, --version    Prints version information
    -v, --verbose    Sets the level of verbosity, which is higher the more times this argument is repeated.

OPTIONS:
    -c, --openmw-cfg <path>    Path to openmw.cfg

SUBCOMMANDS:
    apply      Applies a patch. The source and destination file should be relative to the current directory.
    convert    Converts files from esp to yaml-encoded Delta and vice versa
    deps       Produces a newline separated list of plugin dependencies. Unlike other commands, this supports all
               esp formats.
    diff       Diffs two plugin files and creates a unified text diff representing the difference between the two
               files
    help       Prints this message or the help of the given subcommand(s)
    merge      Creates a merged plugin for the current plugin configuration as defined in openmw.cfg
    size       Prints the number of records in the given plugin. This supports all esp formats.
```

</details>

## Merging Plugins

Merging can be done with the `merge` subcommand: `delta_plugin merge [OPTIONS] [out_file]` (for details see `delta-plugin merge --help`).

DeltaPlugin can merge all record types which can be meaningfully merged (there's no need to use other merging tools as well), and detects removed as well as added content to records such as levelled lists, and similar structures.

Merging is done by using the load order as defined in `openmw.cfg` and creating delta plugins (as described in the next section) for each plugin in `openmw.cfg`, combining them (in order), and storing the records that were changed in a new file.

This solves issues caused by loading multiple mods which change only parts of the same record. As long as the mods change different things, DeltaPlugin can merge the changes so that the resulting record incorporates all changes. Changes to the same part can't be meaningfully resolved except by using just the change from the last mod in the load order that modified that part of the record.

Parallelism can be controlled using the `RAYON_NUM_THREADS` environment variable. E.g. setting `RAYON_NUM_THREADS=1` will force merging to be done in just a single thread, which reduces memory requirements and makes logging output more helpful.

The `--debug-incremental` option dumps the generated deltas to `~/.cache/delta_plugin` (on Linux; on other platforms, increase verbosity and see where the files are being written to). This can be useful if you want to inspect how DeltaPlugin compares files to their masters or when debugging issues with the merge functionality. Note that for a large load order this might produce several gigabytes of data since the yaml format is less compact than the binary esm format.

## Transcoder

The `convert` subcommand can be used to convert esp/esm/omwaddon files into a yaml format which is easier for humans to read and has more widely available software support than the esm format.

Translation can be done from Morrowind-like plugin files (esm, esp, omwaddon, omwgame, etc.) to yaml and back again, including automatic detection of differences between records and their masters.

If the file doesn't contain scripts, the `convert` output will be in a yaml file of the same base name as the original plugin.

If the plugin contains scripts, `convert` will produce a subdirectory called `FILE.d`, which contains `FILE.yaml`, and a `scripts` directory containing a `.mwscript` file for each script. If you use the `--inline` argument, the scripts will instead be inlined into the yaml file, however note that espmarkup currently only includes newlines in strings as `\n` escape characters.

### Delta Records

Unlike their binary counterparts, these files provide the option to modify records
as little or as much as necessary, rather than being forced to replicate information
from overridden records just to modify a single subrecord, or part of a subrecord.

To achieve this, all records are restructured into a more flat format (no multi-value
data subrecords), and have a corresponding "Delta" record which allows all fields except
for the identifier to be omitted, and provides mechanisms for modifying lists and mappings
without replacing the entire list or mapping.

### Patches

DeltaPlugin also supports creating patches for esp/omwaddon files by producing the unified diff of the canonical yaml forms of the plugins (i.e. as produced by the serializer, without extra comments or formatting). While it's possible that it could produce patches in the form of Delta records, certain records (Cell and Dialogue) are treated in an unusual manner by the engine, so the method used to create deltas for these records are not useable for creating patches which apply directly to the file.

*Note that this format is particularly unstable and should probably be replaced with something closer to how DeltaPlugin creates deltas in the first place (which has its own issues, but shouldn't be as sensitive to changes in the formatting of the encoded files).*

## Building

Requires rust 1.60 or newer.

You can install via `cargo install --git https://gitlab.com/bmwinger/delta-plugin`.

You can also install manually by downloading a release tarball or cloning the repository and running `cargo build --release` (which produces the binary in `target/release`.

## Issues

Issues can be reported to either the [GitLab issue tracker](https://gitlab.com/bmwinger/delta-plugin/-/issues) or the [GitLab Email Service Desk](mailto:contact-project+bmwinger-delta-plugin-18372672-issue-@incoming.gitlab.com).
