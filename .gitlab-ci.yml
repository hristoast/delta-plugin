stages:
  - build
  - test
  - prepare
  - upload
  - release

variables:
  PACKAGE_REGISTRY_URL:
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/delta-plugin/${CI_COMMIT_TAG}"
  PACKAGE_LINUX_AMD64: "delta-plugin-${CI_COMMIT_TAG}-linux-amd64.zip"
  PACKAGE_WIN_AMD64: "delta-plugin-${CI_COMMIT_TAG}-windows-amd64.zip"
  PACKAGE_DARWIN_AMD64: "delta-plugin-${CI_COMMIT_TAG}-darwin-amd64.zip"

cache:
  paths:
    - target

rust-minimum:
  stage: build
  image: rust:1.60
  script:
    - cargo build --verbose
    - cargo test --verbose
  allow_failure: true

rust-latest:
  stage: build
  image: rust:latest
  script:
    - cargo build --verbose
    - cargo test --verbose
  allow_failure: true

rust-nightly:
  stage: build
  image: rustlang/rust:nightly
  script:
    - cargo build --verbose
    - cargo test --verbose
  allow_failure: true

lint:
  stage: test
  image: rust:latest
  # allow_failure: true
  script:
    - rustup component add rustfmt clippy
    - cargo fmt -- --check
      #- cargo clippy -- -D warnings # Turn all warnings into errors

# Deploy cargo documentation to gitlab pages, since this can't be submitted to
# crates.io yet.
pages:
  stage: release
  image: rust:latest
  script:
    - cargo doc --no-deps
    - mv target/doc public/
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  artifacts:
    paths:
      - public

prepare_release:
  stage: prepare
  image: rust:alpine
  only:
    - tags
  script:
    - apk add --no-cache musl-dev git zip
    - cargo build --release
    # Generate description from changelog
    - wget https://github.com/tomodian/release/releases/download/0.8.1/release_linux_amd64.zip
    - unzip release_linux_amd64.zip
    - strip target/release/delta_plugin
    - mv target/release/delta_plugin .
    - zip "$PACKAGE_LINUX_AMD64" delta_plugin README.md LICENSE CHANGELOG.md THIRDPARTY.html
  artifacts:
    paths:
      - ${PACKAGE_LINUX_AMD64}
      - release

prepare_release_windows:
  stage: prepare
  tags:
    - windows
  only:
    - tags
  script:
    - choco install rust zip -y
    - cargo build --release
    - mv target/release/delta_plugin.exe .
    - zip "$PACKAGE_WIN_AMD64" delta_plugin.exe README.md LICENSE CHANGELOG.md THIRDPARTY.html
  artifacts:
    paths:
      - ${PACKAGE_WIN_AMD64}

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script: >
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "$PACKAGE_LINUX_AMD64"
      "${PACKAGE_REGISTRY_URL}/${PACKAGE_LINUX_AMD64}"

      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "$PACKAGE_WIN_AMD64"
      "${PACKAGE_REGISTRY_URL}/${PACKAGE_WIN_AMD64}"

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_release
      artifacts: true
    - job: prepare_release_windows
      artifacts: true
  only:
    - tags
  script:
    - echo 'running release_job for $CI_COMMIT_TAG'
    - >
      release-cli create --name "Release $CI_COMMIT_TAG"
      --description "$(./release s -v $CI_COMMIT_TAG | tail -n +2)" --tag-name $CI_COMMIT_TAG
      --ref "$CI_COMMIT_SHA"
      --assets-link "{\"name\":\"${PACKAGE_LINUX_AMD64}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_LINUX_AMD64}\", \"link_type\": \"package\", \"filepath\": \"/${PACKAGE_LINUX_AMD64}\"}"
      --assets-link "{\"name\":\"${PACKAGE_WIN_AMD64}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_WIN_AMD64}\", \"link_type\": \"package\", \"filepath\": \"/${PACKAGE_WIN_AMD64}\"}"
      --assets-link "{\"name\":\"${PACKAGE_DARWIN_AMD64}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_DARWIN_AMD64}\", \"link_type\": \"package\", \"filepath\": \"/${PACKAGE_DARWIN_AMD64}\"}"
