/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */
#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate base64_serde;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate num_derive;
#[macro_use]
extern crate serde_with;
#[macro_use]
extern crate delta_plugin_derive;
#[macro_use]
extern crate log;
#[macro_use]
extern crate nom_derive;
#[macro_use]
extern crate enum_dispatch;
#[macro_use]
extern crate enumflags2;

#[macro_use]
mod delta;
#[macro_use]
mod types;
#[macro_use]
mod util;

mod ai;
mod float;
mod plugin;
mod serialize;
mod version;

mod record;

use crate::plugin::Plugin;
use crate::record::{MetaRecord, RecordType};
use crate::serialize::{apply_delta, create_delta, create_merged, parse_file};
use anyhow::{Context, Result};
use clap::{arg, command, value_parser, Arg, ArgAction, Command};
use esplugin::GameId;
use std::borrow::Cow;
use std::convert::TryInto;
use std::env;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};
use yaml_rust::scanner;
use yaml_rust::{yaml::YamlScalarParser, Yaml, YamlEmitter, YamlLoader};

struct IncludeParser<'a> {
    root: &'a Path,
}

impl<'a> IncludeParser<'a> {
    fn new(root: &'a Path) -> IncludeParser {
        IncludeParser { root }
    }
}

impl<'a> YamlScalarParser for IncludeParser<'a> {
    fn parse_scalar(&self, tag: &scanner::TokenType, value: &str) -> Option<Yaml> {
        if let scanner::TokenType::Tag(ref handle, ref suffix) = *tag {
            if (*handle == "!" || *handle == "yaml-rust.include.prefix") && *suffix == "include" {
                let mut content = String::new();
                return Some(match File::open(self.root.join(value)) {
                    Ok(mut f) => {
                        let _ = f.read_to_string(&mut content);
                        let mut loader = YamlLoader::new();
                        // FIXME: Working dir needs to be updated so that relative paths work
                        // correctly
                        loader.register_scalar_parser(self);
                        match loader.parse_from_str(&content) {
                            Ok(mut docs) => docs.pop().unwrap(),
                            Err(_) => Yaml::BadValue,
                        }
                    }
                    Err(_) => Yaml::BadValue,
                });
            } else if (*handle == "!" || *handle == "yaml-rust.include.prefix")
                && *suffix == "include_str"
            {
                let mut content = String::new();
                return Some(match File::open(self.root.join(value)) {
                    Ok(mut f) => {
                        let _ = f.read_to_string(&mut content);
                        Yaml::String(content)
                    }
                    Err(_) => Yaml::BadValue,
                });
            }
        }
        None
    }
}

fn load_yaml(filename: &Path) -> Result<String> {
    trace!("Parsing {}...", filename.display());
    // Read yaml file
    let mut file = File::open(filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    // Pre-process file to handle include tags
    // Unfortunately this means that line numbers will not be correct
    // Ideally a more tightly integrated include system should be used.
    let canonical_path = Path::new(filename).canonicalize()?;
    let parser = IncludeParser::new(&canonical_path.parent().unwrap());
    let mut loader = YamlLoader::new();
    loader.register_scalar_parser(&parser);
    let combined = loader.parse_from_str(&contents)?;
    let mut contents = String::new();
    let mut emitter = YamlEmitter::new(&mut contents);
    for yaml in combined {
        emitter.dump(&yaml)?;
    }
    Ok(contents)
}

fn load_plugin(filename: &Path) -> Result<Plugin> {
    Ok(match filename.extension().map(|x| x.to_str()) {
        Some(Some("yaml")) => serde_yaml::from_str(&load_yaml(filename)?)
            .context(format!("Failed to parse plugin {}", filename.display()))?,
        Some(_) | None => {
            trace!("Parsing {}...", filename.display());
            parse_file(Path::new(filename), false)
                .context(format!("Failed to parse plugin {}", filename.display()))?
        }
    })
}

fn convert(filename: &Path, inline: bool, compare: bool) -> Result<()> {
    let ext = "yaml";
    match filename.extension().map(|x| x.to_str()) {
        Some(Some("yaml")) => {
            let out_file = filename.with_extension("omwaddon");
            let mut delta_plugin: Plugin = load_plugin(&filename)?;
            delta_plugin.file = out_file.clone();
            trace!("Converting...");
            let masters = serialize::load_masters(&delta_plugin).context(format!(
                "Failed to load masters of plugin {}",
                filename.display()
            ))?;
            let plugin = apply_delta(delta_plugin, &masters.iter().collect::<Vec<_>>());
            let new_plugin: esplugin::Plugin = plugin.try_into().context(format!(
                "Failed to convert plugin {} to esm",
                filename.display()
            ))?;
            println!("Writing to file {}...", out_file.display());
            new_plugin.write()?;
        }
        Some(_) | None => {
            let plugin = load_plugin(&filename)?;
            let out_dir = plugin.file.with_extension("d");

            let mut plugin = if compare {
                let masters = serialize::load_masters(&plugin).context(format!(
                    "Failed to load masters of plugin {}",
                    filename.display()
                ))?;
                let delta_plugin =
                    create_delta(Cow::Owned(plugin), &masters.iter().collect::<Vec<_>>());

                delta_plugin
            } else {
                plugin
            };

            let mut has_script = false;
            if !inline {
                // Check plugin for scripts and write them to a separate file
                for record in &mut plugin.records {
                    if let (id, MetaRecord::Record(RecordType::Script(ref mut script))) = record {
                        has_script = true;
                        script.create_script_file(&id, &out_dir).context(format!(
                            "Failed to create script file for script \"{}\"",
                            id.to_string()
                        ))?;
                    }
                }
            }
            let out_str = serde_yaml::to_string(&plugin).context(format!(
                "Failed to serialize plugin {} as yaml",
                filename.display()
            ))?;

            // Emit includes by replacing the quotes surrounding them.
            let re = regex::Regex::new("\"!(?P<y>\\w*)\\s*\\\\\"(?P<x>[^\"]*)\\\\\"\"").unwrap();
            let out_str = re.replace_all(&out_str, "!$y \"$x\"").into_owned();

            if has_script {
                let file_name = Path::new(plugin.file.file_name().unwrap()).with_extension(ext);

                println!(
                    "Writing result to {}...",
                    out_dir.join(&file_name).display()
                );
                fs::write(out_dir.join(&file_name), out_str)
                    .context(format!("Failed to write file {}", &file_name.display()))?;
            } else {
                let path = plugin.file.with_extension(ext);
                println!("Writing result to {}...", path.display());
                fs::write(&path, out_str)
                    .context(format!("Failed to write file {}", &path.display()))?;
            }
        }
    }
    Ok(())
}

fn diff(original: &Path, modified: &Path) -> Result<String> {
    let mut original = load_plugin(original)?;
    let mut new = load_plugin(modified)?;
    // Clear header fields which aren't helpful to the diff. If they end up being included
    // it may prevent the patch from being applied
    original.header.transpiler_version = None;
    original.header.clear_private();
    new.header.transpiler_version = None;
    new.header.clear_private();

    let original_str = serde_yaml::to_string(&original).context(format!(
        "When serializing {} as yaml",
        original.file.display()
    ))?;

    let new_str = serde_yaml::to_string(&new)
        .context(format!("When serializing {} as yaml", new.file.display()))?;

    let original_file = File::open(&original.file)?;
    let new_file = File::open(&new.file)?;
    let original_time: time::OffsetDateTime = original_file.metadata()?.modified()?.into();
    let new_time: time::OffsetDateTime = new_file.metadata()?.modified()?.into();

    let mut lines: Vec<String> = similar::udiff::unified_diff(
        similar::Algorithm::Patience,
        &original_str,
        &new_str,
        3,
        Some((
            &format!("{}\t{}", original.file.display(), original_time),
            &format!("{}\t{}", modified.display(), new_time),
        )),
    )
    .to_string()
    .lines()
    .map(|x| x.to_string())
    .collect();
    if lines.len() > 0 {
        lines.insert(
            0,
            format!(
                "# Created by DeltaPlugin version {}",
                crate::version::VERSION
            ),
        );
    }
    Ok(lines.join("\n"))
}

fn apply(patch_path: &Path) -> Result<()> {
    let mut patch_file = File::open(patch_path)?;
    let mut patch_string = String::new();
    patch_file.read_to_string(&mut patch_string)?;
    let patch = diffy::Patch::from_str(&patch_string)?;
    let mut original = load_plugin(Path::new(
        patch
            .original()
            .ok_or(anyhow!("Patch is missing output file!"))?,
    ))?;

    original.header.transpiler_version = None;
    original.header.clear_private();

    info!(
        "Applying patch {} to {}",
        patch_path.display(),
        original.file.display()
    );

    let original_string = serde_yaml::to_string(&original).context(format!(
        "Failed to serialize {} as yaml",
        original.file.display()
    ))?;

    let new = diffy::apply(&original_string, &patch)?;
    let new_file_name = patch
        .modified()
        .ok_or(anyhow!("Patch is missing output file!"))?;

    let mut new_plugin: Plugin = serde_yaml::from_str(&new)?;
    new_plugin.file = Path::new(new_file_name).into();
    let bin: esplugin::Plugin = new_plugin.try_into().context(format!(
        "Failed to serialize plugin {} as esm",
        new_file_name
    ))?;
    bin.write()?;
    Ok(())
}

fn get_deps(path: &Path) -> Result<Vec<String>> {
    // Assert file exists and is readable
    fs::metadata(path).context(format!("When reading file \"{}\"", path.display()))?;

    for id in &[
        GameId::Morrowind,
        GameId::Oblivion,
        GameId::Skyrim,
        GameId::Fallout3,
        GameId::FalloutNV,
        GameId::Fallout4,
        GameId::SkyrimSE,
    ] {
        let mut plugin = esplugin::Plugin::new(*id, path);
        if plugin.parse_file(esplugin::ParseMode::HeaderOnly).is_ok() {
            // If the plugin is well-formed, masters should work. It won't fail even if the wrong
            // gameid is being used, since they all use the MAST subrecord, and that's all that's
            // being parsed
            let list = plugin.masters().map_err(|x| {
                anyhow!("Error reading masters for plugin {}: {}", path.display(), x)
            })?;
            return Ok(list);
        }
    }
    return Err(anyhow!(
        "Unable to parse dependencies for file {}",
        path.display()
    ));
}

fn cli() -> Command {
    command!()
        .arg(Arg::new("verbose").short('v').long("verbose").action(ArgAction::Count).help("Sets the level of verbosity,\
            which is higher the more times this argument is repeated").required(false))
        .arg(Arg::new("quiet").short('q').long("quiet").action(ArgAction::SetTrue).help("Run in quiet mode").required(false))
        .arg(Arg::new("openmw-cfg").short('c').value_name("FILE").value_parser(value_parser!(PathBuf)).required(false))
        .subcommand(Command::new("convert")
            .about("Converts files from esp to yaml-encoded Delta and vice versa")
            .arg(arg!([PLUGINS] ... "Files to be converted").value_parser(value_parser!(PathBuf)))
            .arg(arg!(-i --inline "Produces a single file as output, \
            inlining information such as scripts which would normally be written to separate files."))
            .arg(arg!(-c --compare "Don't compare recoords with their masters \
                and instead produce a 1-to-1 representation of the original.
                Only applies when converting esm to yaml")))
        .subcommand(Command::new("merge")
            .about("Creates a merged plugin for the current plugin configuration as defined in openmw.cfg")
            .arg(arg!([out_file] "location of the resulting merged plugin. Default is ./merged.omwaddon")
                .value_parser(value_parser!(PathBuf)))
            .arg(arg!(--"debug-incremental" "Builds in incremental mode, dumping intermediate representations \
            to ~/.cache/delta_plugin").required(false))
            .arg(arg!(--"skip-cells" "Ignore Cell records when creating the merged plugin. Merging cells should \
                be stable, but it is a time and memory-intensive process, so if you have issues with running out of memory \
                you may want to omit cells from the merged plugin.").required(false)))
        .subcommand(Command::new("diff")
            .about("Diffs two plugin files and creates a unified text diff representing the difference between the two files")
            .arg(arg!(<original> "location of the plugin to base the diff off of").value_parser(value_parser!(PathBuf)))
            .arg(arg!(<new> "location of the new version of the plugin").value_parser(value_parser!(PathBuf))))
        .subcommand(Command::new("apply")
            .about("Applies a patch. The source and destination file should be relative to the current directory.")
            .arg(arg!(<patch> "patch file to apply").value_parser(value_parser!(PathBuf))))
        .subcommand(Command::new("deps")
            .about("Produces a newline separated list of plugin dependencies. Unlike other commands, this supports all esp formats.")
            .arg(arg!(<plugin> "Plugin to parse. Should be a binary esp/esm/esl of any type.").value_parser(value_parser!(PathBuf))))
        .subcommand(Command::new("size")
            .about("Prints the number of records in the given plugin. This supports all esp formats.")
            .arg(arg!(<plugin> "Plugin to parse. Should be a binary esp/esm/esl of any type.").value_parser(value_parser!(PathBuf))))
}

#[test]
fn verify_cli() {
    cli().debug_assert();
}

fn main() -> Result<()> {
    let matches = cli().get_matches();
    // TODO: Additional command line options, such as specifying the location of the cache directory,
    // specifying explicit paths of the plugins to be merged and overriding the encoding to be used
    // when parsing plugin files.

    let verbosity = if matches.get_flag("quiet") {
        0
    } else {
        1 + matches.get_count("verbose")
    };

    stderrlog::new()
        .module(module_path!())
        .verbosity(verbosity as usize)
        .init()
        .unwrap();

    if let Some(path) = matches.get_one::<PathBuf>("openmw-cfg") {
        env::set_var("OPENMW_CONFIG", path);
    }

    if let Some(matches) = matches.subcommand_matches("convert") {
        if let Some(plugins) = matches.get_many::<PathBuf>("PLUGINS") {
            for filename in plugins {
                convert(
                    filename,
                    matches.get_flag("inline"),
                    matches.get_flag("compare"),
                )?;
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("diff") {
        let original = matches
            .get_one::<PathBuf>("original")
            .ok_or(anyhow!("Missing argument <original>"))?;
        let new = matches
            .get_one::<PathBuf>("new")
            .ok_or(anyhow!("Missing argument <new>"))?;

        let result = diff(original, new)?;
        if !result.is_empty() {
            println!("{}", result);
        }
    } else if let Some(matches) = matches.subcommand_matches("apply") {
        apply(
            &matches
                .get_one::<PathBuf>("patch")
                .ok_or(anyhow!("Missing argument <patch>"))?,
        )?;
    } else if let Some(matches) = matches.subcommand_matches("merge") {
        let filename: PathBuf = matches
            .get_one::<PathBuf>("out_file")
            .unwrap_or(
                &std::env::current_dir()
                    .context("Current directory could not be read.")?
                    .join("merged.omwaddon"),
            )
            .to_path_buf();
        let merged = create_merged(
            &filename,
            matches.get_flag("debug-incremental"),
            matches.get_flag("skip-cells"),
            verbosity > 1,
        )
        .context(format!(
            "Failed to create merged plugin {}",
            filename.display()
        ))?;
        println!("Writing merged plugin to {}", filename.display());
        let bin: esplugin::Plugin = merged.try_into().context(format!(
            "Failed to serialize plugin {} as esm",
            filename.display()
        ))?;
        bin.write()
            .context(format!("Failed to write file {}", filename.display()))?;
        println!("Done!");
    } else if let Some(matches) = matches.subcommand_matches("deps") {
        let path = matches.get_one::<PathBuf>("plugin").unwrap();
        for dep in get_deps(path)? {
            println!("{}", dep);
        }
    } else if let Some(matches) = matches.subcommand_matches("size") {
        let filename = matches.get_one::<PathBuf>("plugin").unwrap();
        for id in &[
            GameId::Morrowind,
            GameId::Oblivion,
            GameId::Skyrim,
            GameId::Fallout3,
            GameId::FalloutNV,
            GameId::Fallout4,
            GameId::SkyrimSE,
        ] {
            let mut plugin = esplugin::Plugin::new(*id, Path::new(&filename));
            if plugin.parse_file(esplugin::ParseMode::HeaderOnly).is_ok() {
                if let Some(records) = plugin.record_and_group_count() {
                    println!("{}", records);
                    break;
                }
            }
        }
    }

    Ok(())
}
