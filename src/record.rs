/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::delta::Delta;
use crate::types::{EffectType, Point2, SkillType};
use crate::util::{take_nt_str, RecordError};
use derive_more::{Constructor, Display, From};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
use std::str::FromStr;

pub mod activator;
pub mod apparatus;
pub mod armour;
pub mod birthsign;
pub mod body_part;
pub mod book;
pub mod cell;
pub mod cell_ref;
pub mod class;
pub mod clothing;
pub mod container;
pub mod creature;
pub mod dialogue;
pub mod door;
pub mod effect;
pub mod enchantment_effect;
pub mod faction;
pub mod gamesetting;
pub mod global;
pub mod header;
pub mod ingredient;
pub mod land_texture;
pub mod landscape;
pub mod levelled_list;
pub mod light;
pub mod lockpick;
pub mod magic_effect;
pub mod misc;
pub mod npc;
pub mod path_grid;
pub mod potion;
pub mod probe;
pub mod race;
pub mod region;
pub mod repair;
pub mod script;
pub mod skill;
pub mod sound;
pub mod sound_generator;
pub mod spell;
pub mod startup;
pub mod stat;
pub mod weapon;

use activator::{Activator, DeltaActivator};
use apparatus::{Apparatus, DeltaApparatus};
use armour::{Armour, DeltaArmour};
use birthsign::{BirthSign, DeltaBirthSign};
use body_part::{BodyPart, DeltaBodyPart};
use book::{Book, DeltaBook};
use cell::{Cell, DeltaCell};
use class::{Class, DeltaClass};
use clothing::{Clothing, DeltaClothing};
use container::{Container, DeltaContainer};
use creature::{Creature, DeltaCreature};
use dialogue::{DeltaDialogue, Dialogue};
use door::{DeltaDoor, Door};
use enchantment_effect::{DeltaEnchantmentEffect, EnchantmentEffect};
use faction::{DeltaFaction, Faction};
use gamesetting::GameSetting;
use global::Global;
use ingredient::{DeltaIngredient, Ingredient};
use land_texture::LandTexture;
use landscape::Landscape;
use levelled_list::{DeltaLevelledCreatures, DeltaLevelledItems, LevelledCreatures, LevelledItems};
use light::{DeltaLight, Light};
use lockpick::{DeltaLockpick, Lockpick};
use magic_effect::{DeltaMagicEffect, MagicEffect};
use misc::{DeltaMisc, Misc};
use npc::{DeltaNPC, NPC};
use path_grid::PathGrid;
use potion::{DeltaPotion, Potion};
use probe::{DeltaProbe, Probe};
use race::{DeltaRace, Race};
use region::{DeltaRegion, Region};
use repair::{DeltaRepair, Repair};
use script::Script;
use skill::{DeltaSkill, Skill};
use sound::Sound;
use sound_generator::{DeltaSoundGenerator, SoundGenerator};
use spell::{DeltaSpell, Spell};
use startup::Startup;
use stat::Static;
use strum::EnumString;
use weapon::{DeltaWeapon, Weapon};

pub trait Id<T> {
    fn get_id(subrecords: &[esplugin::Subrecord]) -> Result<T, RecordError>;
}

#[derive(Clone, Debug, Constructor)]
pub struct Pair<K, T> {
    pub id: K,
    pub value: T,
}

#[derive(Clone, Debug, Constructor)]
pub struct RecordPair<T: TryInto<MetaRecord>> {
    pub id: RecordId,
    pub record: T,
}

pub trait FullRecord {
    fn get_patch(&self, master: &RecordType) -> Option<MetaRecord>;
    fn apply_patch(&mut self, other: MetaRecord);
}

macro_rules! decl_records {
    (
        ($($record:ident = $record_type_str:expr,)*),
        ($($delta_record:ident,) *),
        ($($dele_record:ident = $dele_record_type_str:expr,)*),
    ) => {
        #[derive(Display, Copy, Clone, Hash, Debug, Eq, PartialEq, Serialize, Deserialize, EnumString)]
        pub enum RecordTypeName {
            $($record,)*
        }

        #[enum_dispatch]
        #[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
        #[serde(tag = "type")]
        pub enum RecordType {
            $($record,)*
        }

        #[derive(Clone, Debug, Serialize, Deserialize)]
        #[serde(untagged)]
        pub enum MetaRecord {
            Delta(DeltaRecordType),
            Record(RecordType),
            Delete,
            Unknown,
        }

        #[enum_dispatch]
        #[derive(Clone, Debug, Serialize, Deserialize)]
        #[serde(tag = "type")]
        pub enum DeltaRecordType {
            $( $delta_record, )*
        }

        impl DeltaRecordType {
            pub fn is_empty(&self) -> bool {
                match self {
                    $(DeltaRecordType::$delta_record(record) => record.is_empty(),)*
                }
            }
        }

        impl TryInto<esplugin::Record> for RecordPair<MetaRecord> {
            type Error = RecordError;
            fn try_into(self) -> Result<esplugin::Record, RecordError> {
                match self.record {
                    $(MetaRecord::Record(RecordType::$record(record)) => {
                        Ok(RecordPair::new(self.id, record).try_into()?)
                    })*
                    _ => unimplemented!()
                }
            }
        }

        $(
            impl Into<MetaRecord> for $record {
                fn into(self) -> MetaRecord {
                    MetaRecord::Record(RecordType::$record(self))
                }
            }
        )*

        $(
            impl Into<MetaRecord> for $delta_record {
                fn into(self) -> MetaRecord {
                    MetaRecord::Delta(DeltaRecordType::$delta_record(self))
                }
            }
        )*

        /*impl TryInto<esplugin::Record> for RecordType {
            type Error = RecordError;
            fn try_into(self) -> Result<esplugin::Record, RecordError> {
                use self::RecordType::*;
                match self {
                    $($record(x) => Ok(x.try_into()?),)*
                }
            }
        }*/

        /*
        impl TryInto<esplugin::Record> for MetaRecord {
            type Error = RecordError;
            fn try_into(self) -> Result<esplugin::Record, RecordError> {
                match self {
                    MetaRecord::Record(record) => Ok(record.try_into()?),
                    // TODO: Serialize delete records
                    MetaRecord::Delete => unimplemented!(),
                    // FIXME: Fail gracefully
                    MetaRecord::Delta(_) => panic!("Cannot serialize delta records as Esplugin records!"),
                    MetaRecord::Unknown => panic!("Cannot serialize unknown records!"),
                }
            }
        }*/

        impl RecordType {
            pub fn get_full_record<'a>(&'a self) -> Box<&'a dyn FullRecord> {
                use self::RecordType::*;
                match self {
                    $($record(x) => Box::new(x),)*
                }
            }

            pub fn get_full_record_mut<'a>(&'a mut self) -> Box<&'a mut dyn FullRecord> {
                use self::RecordType::*;
                match self {
                    $($record(x) => Box::new(x),)*
                }
            }
        }

        fn is_delete_record(record: &esplugin::Record) -> bool {
            if &record.header_type() != b"CELL" {
                return record
                    .subrecords()
                    .iter()
                    .find(|subrecord| match subrecord.subrecord_type() {
                        b"DELE" => true,
                        _ => false,
                    })
                    .is_some();
            } else {
                // Cell records contain DELE subrecords for their references.
                // If we reach a FRMR subrecord, then we're just parsing references and there is no
                // DELE subrecord for the record.
                for subrecord in record.subrecords() {
                    if subrecord.subrecord_type() == b"DELE" {
                        return true;
                    } else if subrecord.subrecord_type() == b"FRMR" {
                        return false;
                    }
                }
                return false;
            }
        }


        impl<'a> TryFrom<esplugin::Record> for RecordPair<MetaRecord> {
            type Error = RecordError;
            fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
                if is_delete_record(&record) {
                    let recordid: RecordId = match &record.header_type() {
                        $($dele_record_type_str => {
                            let mut id = None;
                            for subrecord in record.subrecords() {
                                if b"NAME" == subrecord.subrecord_type() {
                                    id = Some(RecordId::String(RecordTypeName::$dele_record, take_nt_str(subrecord.data())?.1));
                                }
                            }
                            id.ok_or(RecordError::MissingSubrecord("This DeleteRecord doesn't have a NAME subrecord!"))?
                        })*
                        // Note that SKIL, MGEF, and GMST records can't be deleted
                        _ => Err(RecordError::UnknownRecordType(record.header_type()))?,
                    };
                    Ok(RecordPair::new(recordid, MetaRecord::Delete))
                } else {
                    match &record.header_type() {
                        $($record_type_str => {
                            let pair: RecordPair<$record> = record.try_into()?;
                            Ok(RecordPair::new(pair.id.into(), MetaRecord::Record(RecordType::$record(pair.record))))
                        })*
                        _ => Err(RecordError::UnknownRecordType(record.header_type())),
                    }
                }
            }
        }
    }
}

decl_records! {
    ( // All Records
        Activator = b"ACTI",
        Apparatus = b"APPA",
        Armour = b"ARMO",
        BirthSign = b"BSGN",
        BodyPart = b"BODY",
        Book = b"BOOK",
        Cell = b"CELL",
        Class = b"CLAS",
        Clothing = b"CLOT",
        Container = b"CONT",
        Creature = b"CREA",
        Dialogue = b"DIAL",
        // DialogueInfo = b"INFO",
        Door = b"DOOR",
        EnchantmentEffect = b"ENCH",
        Faction = b"FACT",
        GameSetting = b"GMST",
        Global = b"GLOB",
        Ingredient = b"INGR",
        Landscape = b"LAND",
        LandTexture = b"LTEX",
        LevelledCreatures = b"LEVC",
        LevelledItems = b"LEVI",
        Light = b"LIGH",
        Lockpick = b"LOCK",
        MagicEffect = b"MGEF",
        Misc = b"MISC",
        NPC = b"NPC_",
        PathGrid = b"PGRD",
        Potion = b"ALCH",
        Probe = b"PROB",
        Race = b"RACE",
        Region = b"REGN",
        Repair = b"REPA",
        Script = b"SCPT",
        Skill = b"SKIL",
        Sound = b"SOUN",
        SoundGenerator = b"SNDG",
        Spell = b"SPEL",
        Startup = b"SSCR",
        Static = b"STAT",
        Weapon = b"WEAP",
    ),
    ( // Delta Records
        DeltaActivator,
        DeltaApparatus,
        DeltaArmour,
        DeltaBirthSign,
        DeltaBodyPart,
        DeltaBook,
        DeltaCell,
        DeltaClass,
        DeltaClothing,
        DeltaContainer,
        DeltaCreature,
        DeltaDialogue,
        DeltaDoor,
        DeltaEnchantmentEffect,
        DeltaFaction,
        DeltaIngredient,
        DeltaLevelledCreatures,
        DeltaLevelledItems,
        DeltaLight,
        DeltaLockpick,
        DeltaMagicEffect,
        DeltaMisc,
        DeltaNPC,
        DeltaPotion,
        DeltaProbe,
        DeltaRace,
        DeltaRegion,
        DeltaRepair,
        DeltaSkill,
        DeltaSoundGenerator,
        DeltaSpell,
        DeltaWeapon,
    ),
    ( // Deletable Records
        Activator = b"ACTI",
        Apparatus = b"APPA",
        Armour = b"ARMO",
        BirthSign = b"BSGN",
        BodyPart = b"BODY",
        Book = b"BOOK",
        Cell = b"CELL",
        Class = b"CLAS",
        Clothing = b"CLOT",
        Container = b"CONT",
        Creature = b"CREA",
        Dialogue = b"DIAL",
        // DialogueInfo = "INFO", Technically deletable, but can't be deleted in this scope since
        // they are bundled in with DIAL records
        Door = b"DOOR",
        EnchantmentEffect = b"ENCH",
        Faction = b"FACT",
        Global = b"GLOB",
        Ingredient = b"INGR",
        Landscape = b"LAND",
        LandTexture = b"LTEX",
        LevelledCreatures = b"LEVC",
        LevelledItems = b"LEVI",
        Light = b"LIGH",
        Lockpick = b"LOCK",
        Misc = b"MISC",
        NPC = b"NPC_",
        PathGrid = b"PGRD",
        Potion = b"ALCH",
        Probe = b"PROB",
        Race = b"RACE",
        Region = b"REGN",
        Repair = b"REPA",
        Script = b"SCPT",
        Sound = b"SOUN",
        SoundGenerator = b"SNDG",
        Spell = b"SPEL",
        Startup = b"SSCR",
        Static = b"STAT",
        Weapon = b"WEAP",
    ),
}

#[derive(
    Display, Hash, Eq, PartialEq, Debug, Clone, From, DeserializeFromStr, SerializeDisplay,
)]
pub enum RecordId {
    #[display(fmt = "{}::{}", _0, _1)]
    String(RecordTypeName, String),
    /// There exists at least one dialogue topic which shares its identifier with a Cell
    /// While determining exactly which identifiers are in the global namespace would be
    /// useful, (e.g. things that can be referenced as CellRefs) these issues can probably
    /// be handled on an as-needed basis since modders should be encouraged namespace their
    /// records and avoid name conflicts.
    /// In this case, the identifier may be what is displayed in-game,
    /// so it's hard to work around, though the offending cell id could have been different
    #[display(fmt = "skill::{}", _0)]
    SkillType(SkillType),
    #[display(fmt = "mgef::{}", _0)]
    MagicEffect(EffectType),
    #[display(fmt = "cell::{}x{}", "_0.x", "_0.y")]
    #[from(ignore)]
    ExteriorCell(Point2<i32>),
    #[display(fmt = "landscape::{}x{}", "_0.x", "_0.y")]
    #[from(ignore)]
    Landscape(Point2<i32>),
    #[display(fmt = "path_grid::{}x{}", "_0.x", "_0.y")]
    #[from(ignore)]
    ExteriorPathGrid(Point2<i32>),
}

#[derive(From, Debug, Display)]
pub enum RecordIdParseError {
    ParseIntError(std::num::ParseIntError),
    ParseEnumVariantError(enum_from_str::ParseEnumVariantError),
    #[display(
        fmt = "Invalid record id {}. Note that record ids must include the record type in the form \"RecordType::id\"",
        "_0"
    )]
    InvalidRecordIdFormat(String),
    StrumParseError(strum::ParseError),
}

impl FromStr for RecordId {
    type Err = RecordIdParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let cell_re = Regex::new(r"cell::(?P<x>-?\d+)x(?P<y>-?\d+)").unwrap();
        let land_re = Regex::new(r"landscape::(?P<x>-?\d+)x(?P<y>-?\d+)").unwrap();
        let skill_re = Regex::new(r"skill::(?P<name>.+)").unwrap();
        let mgef_re = Regex::new(r"mgef::(?P<name>.+)").unwrap();
        let path_grid_ext_re = Regex::new(r"path_grid::(?P<x>-?\d+)x(?P<y>-?\d+)").unwrap();
        let string_re = Regex::new(r"(?P<type>.+?)::(?P<name>.*)").unwrap();
        if let Some(captures) = cell_re.captures(s) {
            Ok(RecordId::ExteriorCell(Point2::new(
                captures.name("x").unwrap().as_str().parse()?,
                captures.name("y").unwrap().as_str().parse()?,
            )))
        } else if let Some(captures) = land_re.captures(s) {
            Ok(RecordId::Landscape(Point2::new(
                captures.name("x").unwrap().as_str().parse()?,
                captures.name("y").unwrap().as_str().parse()?,
            )))
        } else if let Some(captures) = path_grid_ext_re.captures(s) {
            Ok(RecordId::ExteriorPathGrid(Point2::new(
                captures.name("x").unwrap().as_str().parse()?,
                captures.name("y").unwrap().as_str().parse()?,
            )))
        } else if let Some(captures) = skill_re.captures(s) {
            Ok(RecordId::SkillType(
                captures.name("name").unwrap().as_str().parse()?,
            ))
        } else if let Some(captures) = mgef_re.captures(s) {
            Ok(RecordId::MagicEffect(
                captures.name("name").unwrap().as_str().parse()?,
            ))
        } else if let Some(captures) = string_re.captures(s) {
            Ok(RecordId::String(
                RecordTypeName::from_str(captures.name("type").unwrap().as_str())?,
                captures.name("name").unwrap().as_str().to_string(),
            ))
        } else {
            Err(RecordIdParseError::InvalidRecordIdFormat(s.to_string()))
        }
    }
}

impl TryInto<String> for RecordId {
    type Error = RecordError;
    fn try_into(self) -> Result<String, RecordError> {
        match self {
            RecordId::String(_, string) => Ok(string),
            x => Err(RecordError::InvalidId(x)),
        }
    }
}

impl<'a> TryInto<&'a String> for &'a RecordId {
    type Error = RecordError;
    fn try_into(self) -> Result<&'a String, RecordError> {
        match self {
            RecordId::String(_, string) => Ok(string),
            x => Err(RecordError::InvalidId(x.clone())),
        }
    }
}

impl TryInto<EffectType> for RecordId {
    type Error = RecordError;
    fn try_into(self) -> Result<EffectType, RecordError> {
        match self {
            RecordId::MagicEffect(et) => Ok(et),
            x => Err(RecordError::InvalidId(x)),
        }
    }
}

impl TryInto<SkillType> for RecordId {
    type Error = RecordError;
    fn try_into(self) -> Result<SkillType, RecordError> {
        match self {
            RecordId::SkillType(x) => Ok(x),
            x => Err(RecordError::InvalidId(x)),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::record::{RecordId, RecordTypeName};
    use crate::types::{EffectType, Point2, SkillType};
    use std::str::FromStr;
    use test_case::test_case;

    #[test_case("Dialogue::SomeDialogue", RecordId::String(RecordTypeName::Dialogue, "SomeDialogue".to_string()) ; "nonempty dialogue")]
    #[test_case("Dialogue::", RecordId::String(RecordTypeName::Dialogue, String::new()) ; "empty dialogue")]
    #[test_case("skill::Block", RecordId::SkillType(SkillType::Block) ; "block skill")]
    #[test_case("mgef::WaterBreathing", RecordId::MagicEffect(EffectType::WaterBreathing) ; "water breathing effect")]
    #[test_case("cell::0x0", RecordId::ExteriorCell(Point2::new(0, 0)) ; "exterior cell")]
    #[test_case("landscape::-1x-1", RecordId::Landscape(Point2::new(-1, -1)) ; "landscape")]
    #[test_case("path_grid::19x102", RecordId::ExteriorPathGrid(Point2::new(19, 102)) ; "exterior pathgrid")]
    fn record_id_to_from_string(string: &str, recordid: RecordId) {
        assert_eq!(RecordId::from_str(string).unwrap(), recordid);
        assert_eq!(string, &recordid.to_string());
    }
}
