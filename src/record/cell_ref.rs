/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{Id, Pair};
use crate::types::{take_position, Position};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, record_type, take_nt_str, RecordError,
};
use derive_more::Constructor;
use esplugin::Subrecord;
use nom::number::complete::{le_f32, le_i32, le_i8, le_u24, le_u32};
use nom::sequence::tuple;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
use std::str::FromStr;

fn one() -> f64 {
    1.0
}

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub enum CellRefMaster {
    // FIXME: We should be able to simplify the ids for local cell references so that they don't
    // include the full plugin name.
    // However other plugins should be able to reference them as an External
    //Local,
    Unknown,
    External(String),
}

#[derive(Constructor, Hash, Eq, PartialEq, Debug, Clone, DeserializeFromStr, SerializeDisplay)]
/// The identifier for cell references.
///
/// Includes both the plugin which originally provided the
/// reference, and a unique numeric identifier
pub struct CellRefId {
    id: u32,
    master: CellRefMaster,
}

impl std::fmt::Display for CellRefId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.master {
            //CellRefMaster::Local => write!(f, "cellref::{}", self.id),
            CellRefMaster::Unknown => write!(f, "::<unknown>::cellref::{}", self.id),
            CellRefMaster::External(master) => write!(f, "::{}::cellref::{}", master, self.id),
        }
    }
}

impl FromStr for CellRefId {
    type Err = std::num::ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"::((?P<m>.+)::)?cellref::(?P<i>-?\d+)").unwrap();
        if let Some(captures) = re.captures(s) {
            let master = captures.name("m").unwrap();
            let master = if master.as_str() == "<unknown>" {
                CellRefMaster::Unknown
            } else {
                CellRefMaster::External(master.as_str().to_string().to_lowercase())
            };
            Ok(CellRefId::new(
                captures.name("i").unwrap().as_str().parse()?,
                master,
            ))
        } else {
            panic!("{} is not a CellRefId!", s)
        }
    }
}

impl CellRefId {
    fn get_master_index(self) -> Result<i32, RecordError> {
        match self.master {
            CellRefMaster::External(master) => crate::plugin::PLUGINS.with(|v| {
                let lowered = master.to_lowercase();
                v.borrow()
                    .iter()
                    .position(|r| r == &lowered)
                    .map(|x| x as i32)
                    .ok_or(RecordError::UnknownMaster(master))
            }),
            CellRefMaster::Unknown => Ok(-1),
        }
    }

    pub fn is_index_valid(index: i32) -> bool {
        let length = crate::plugin::PLUGINS.with(|v| v.borrow().len());
        (length as i32) > index && index >= 0
    }

    pub fn get_master_from_index(index: i32) -> CellRefMaster {
        if index < 0 {
            CellRefMaster::Unknown
        } else {
            CellRefMaster::External(
                crate::plugin::PLUGINS.with(|v| v.borrow()[index as usize].clone()),
            )
        }
    }
}

impl TryInto<[u8; 8]> for CellRefId {
    type Error = RecordError;
    fn try_into(self) -> Result<[u8; 8], RecordError> {
        let mut arr = [0u8; 8];
        let first = self.id.to_le_bytes();
        let second = (self.get_master_index()? as i32).to_le_bytes();
        arr[0..4].clone_from_slice(&first);
        arr[4..8].clone_from_slice(&second);
        Ok(arr)
    }
}

impl TryInto<[u8; 4]> for CellRefId {
    type Error = RecordError;
    fn try_into(self) -> Result<[u8; 4], RecordError> {
        // We'll ignore the most significant byte to make it a le_u24
        let first = self.id.to_le_bytes();
        let second = (self.get_master_index()? as i8).to_le_bytes();
        Ok([first[0], first[1], first[2], second[0]])
    }
}

impl TryFrom<&[u8]> for CellRefId {
    type Error = RecordError;
    fn try_from(data: &[u8]) -> Result<CellRefId, RecordError> {
        let (id, master) = if data.len() == 8 {
            // 8 bytes: 4 bytes id, 4 bytes content
            let (idnum, master_index) = tuple((le_u32, le_i32))(data)?.1;
            (idnum, master_index)
        } else {
            // 4 bytes: 24 bits id, 4 bits content
            let (idnum, master_index) = tuple((le_u24, le_i8))(data)?.1;
            // If index does not appear to match, assume it's just an identifier.
            // FIXME: Verify.
            // This currently matches openmw's behaviour, but it seems fishy.
            if Self::is_index_valid(master_index as i32) {
                (idnum as u32, master_index as i32)
            } else {
                if master_index != 0 {
                    debug!(
                        "Master index {} was invalid for reference {}. Treating as a local reference",
                        master_index,
                        le_u32(data)?.1
                    );
                }
                (le_u32(data)?.1, 0)
            }
        };
        let result = CellRefId::new(id, CellRefId::get_master_from_index(master));
        Ok(result)
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct Teleport {
    pub dest: Position,
    /// Destination cell when teleporting (FIXME: If not present, does it mean the destination is
    /// inside the current cell?)
    pub cell: Option<String>,
}

/// FIXME: Many of these are unique to particular types of items. We really should enforce their
/// type and only allow fields that make sense for that item.
#[skip_serializing_none]
#[derive(DeltaRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize, Default)]
pub struct CellRef {
    /// Identifier of the object being referenced
    pub object_id: String,
    /// Scale of the object
    #[serde(default = "one")]
    pub scale: f64,
    /// NPC Who owns this object (and will be angry if you steal it)
    pub owner: Option<String>,
    /// Global variable which, if set, enables this item. E.g. a rented bed.
    pub global_variable: Option<String>,
    /// Id of the creature trapped inside this soul gem
    pub soul: Option<String>,
    /// The faction who owns this object
    pub faction: Option<String>,
    /// The faction rank required to use this object. -1 means any rank
    /// (FIXME: This could be better constrained. Perhaps an enum?)
    pub faction_rank_req: Option<i32>,
    /// Remaining charge or health
    pub charge: Option<i32>,
    /// Remaining enchantment charge
    pub enchantment_charge: Option<f64>,
    /// Value of a gold item (e.g. piles of 5 or 10)
    pub gold_value: Option<u64>,
    /// For doors to indicate a teleport location. If not present, the door opens via animation.
    pub teleport: Option<Teleport>,
    /// Level of the lock for containers and doors
    pub lock_level: Option<u32>,
    /// Key identifier
    pub key: Option<String>,
    /// Trap identifier
    pub trap: Option<String>,
    /// If set prevents editing the reference in the construction set
    /// FIXME: Should this even be included. It would have no function for editing the text file
    /// directly, but could still be used for editors like the construction set.
    pub reference_blocked: bool,
    /// Position of the object within the cell
    pub position: Option<Position>,
}

impl Id<CellRefId> for CellRef {
    fn get_id(subrecords: &[esplugin::Subrecord]) -> Result<CellRefId, RecordError> {
        let mut id: Option<CellRefId> = None;
        for subrecord in subrecords {
            if let b"FRMR" = subrecord.subrecord_type() {
                id = Some(subrecord.data().try_into()?);
                break;
            }
        }
        id.ok_or(RecordError::MissingSubrecord("FRMR"))
    }
}

impl TryFrom<&[esplugin::Subrecord]> for Pair<CellRefId, CellRef> {
    type Error = RecordError;
    fn try_from(subrecords: &[Subrecord]) -> Result<Self, RecordError> {
        let mut id: Option<CellRefId> = None;
        let mut object_id = None;
        let mut scale = 1.0;
        let mut owner = None;
        let mut global_variable = None;
        let mut reference_blocked = false;
        let mut soul = None;
        let mut faction = None;
        let mut faction_rank_req = None;
        let mut teleport_dest = None;
        let mut teleport_cell = None;
        let mut lock_level = None;
        let mut enchantment_charge = None;
        let mut charge = None;
        let mut gold_value = None;
        let mut position = None;
        let mut key = None;
        let mut trap = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => object_id = Some(take_nt_str(data)?.1),
                b"ANAM" => owner = Some(take_nt_str(data)?.1),
                b"BNAM" => global_variable = Some(take_nt_str(data)?.1),
                b"UNAM" => reference_blocked = le_i8(data)?.1 != -1,
                b"XSOL" => soul = Some(take_nt_str(data)?.1),
                b"CNAM" => faction = Some(take_nt_str(data)?.1),
                b"INDX" => faction_rank_req = Some(le_i32(data)?.1),
                b"XCHG" => enchantment_charge = Some(parse_float(le_f32(data)?.1)),
                b"INTV" => charge = Some(le_i32(data)?.1),
                b"NAM9" => gold_value = Some(le_i32(data)?.1 as u64),
                b"DODT" => teleport_dest = Some(take_position(data)?.1),
                b"DATA" => position = Some(take_position(data)?.1),
                b"DNAM" => teleport_cell = Some(take_nt_str(data)?.1),
                b"FLTV" => lock_level = Some(le_i32(data)?.1 as u32),
                b"KNAM" => key = Some(take_nt_str(data)?.1),
                b"TNAM" => trap = Some(take_nt_str(data)?.1),
                b"FRMR" => id = Some(data.try_into()?),
                b"XSCL" => scale = parse_float(le_f32(data)?.1),
                b"NAM0" => (),
                x => return Err(RecordError::UnexpectedSubrecord(*x)),
            }
        }

        let object_id = object_id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let id = id.ok_or(RecordError::MissingSubrecord("FRMR"))?;
        let cell_ref = CellRef {
            object_id,
            scale,
            owner,
            global_variable,
            soul,
            faction,
            faction_rank_req,
            charge,
            enchantment_charge,
            gold_value,
            teleport: teleport_dest.map(|dest| Teleport {
                dest: dest,
                cell: teleport_cell,
            }),
            lock_level,
            key,
            trap,
            reference_blocked,
            position,
        };
        Ok(Pair::new(id, cell_ref))
    }
}

impl TryInto<Vec<esplugin::Subrecord>> for Pair<CellRefId, CellRef> {
    type Error = RecordError;
    fn try_into(self) -> Result<Vec<esplugin::Subrecord>, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let record = self.value;

        let iddata: [u8; 4] = self.id.try_into()?;
        subrecords.push(Subrecord::new(*b"FRMR", iddata.to_vec(), false));
        push_str_subrecord(&mut subrecords, &record.object_id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &record.owner, "ANAM")?;
        push_opt_str_subrecord(&mut subrecords, &record.global_variable, "BNAM")?;
        push_opt_str_subrecord(&mut subrecords, &record.soul, "XSOL")?;
        push_opt_str_subrecord(&mut subrecords, &record.faction, "CNAM")?;
        push_opt_str_subrecord(&mut subrecords, &record.key, "KNAM")?;
        push_opt_str_subrecord(&mut subrecords, &record.trap, "TNAM")?;
        if record.reference_blocked {
            subrecords.push(Subrecord::new(*b"UNAM", 1i8.to_le_bytes().to_vec(), false));
        }
        if let Some(faction_rank_req) = record.faction_rank_req {
            subrecords.push(Subrecord::new(
                *b"INDX",
                (faction_rank_req as i32).to_le_bytes().to_vec(),
                false,
            ));
        }
        if let Some(enchantment_charge) = record.enchantment_charge {
            subrecords.push(Subrecord::new(
                *b"XCHG",
                (enchantment_charge as f32).to_le_bytes().to_vec(),
                false,
            ));
        }
        if let Some(charge) = record.charge {
            subrecords.push(Subrecord::new(
                *b"INTV",
                (charge as i32).to_le_bytes().to_vec(),
                false,
            ));
        }
        if let Some(gold) = record.gold_value {
            subrecords.push(Subrecord::new(
                record_type("NAM9"),
                (gold as i32).to_le_bytes().to_vec(),
                false,
            ));
        }
        if let Some(teleport) = record.teleport {
            subrecords.push(teleport.dest.into_subrecord(b"DODT")?);
            push_opt_str_subrecord(&mut subrecords, &teleport.cell, "DNAM")?;
        }
        if let Some(position) = record.position {
            subrecords.push(position.into_subrecord(b"DATA")?);
        }
        if let Some(lock_level) = record.lock_level {
            subrecords.push(Subrecord::new(
                *b"FLTV",
                (lock_level as i32).to_le_bytes().to_vec(),
                false,
            ));
        }
        if record.scale != 1.0 {
            subrecords.push(Subrecord::new(
                *b"XSCL",
                (record.scale as f32).to_le_bytes().to_vec(),
                false,
            ));
        }

        Ok(subrecords)
    }
}
