/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::delta::{create_map_vanilla_diff, Deletable};
use crate::record::cell_ref::{CellRef, CellRefId, DeltaCellRef};
use crate::record::{Id, Pair, RecordId, RecordPair, RecordTypeName};
use crate::types::{take_rgba_colour, Point2, RGBAColour};
use crate::util::{
    is_default, parse_float, push_opt_str_subrecord, push_str_subrecord, record_type,
    subrecords_len, take_nt_str, RecordError,
};
use derive_more::{Constructor, Display};
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::{LinkedHashMap, LinkedHashSet};
use nom::number::complete::{le_f32, le_i32};
use nom::sequence::tuple;
use nom_derive::Parse;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum CellTraits {
    Interior = 0x01,
    HasWater = 0x02,
    NoSleep = 0x04,
    Unknown32 = 0x20,
    Unknown64 = 0x40,
    QuasiExterior = 0x80,
}

#[derive(Clone, Display, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum CellId {
    Interior(String),
    Exterior(Point2<i32>),
}

/// Game world cells, either interior or exterior
/// FIXME:
/// 1. Can we omit location for interior cells?
/// 2. Is region needed for interior cells?
/// 3. What is name used for in exterior cells? (usually is empty)
/// When this is done we can also probably get rid of the Interior trait (though we'll
/// still need to parse it from the ESP obviously), as the ID already encodes this
#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[ignore_missing]
pub struct Cell {
    /// Interior Cell Name
    #[serde(default, skip_serializing_if = "is_default")]
    pub name: String,
    /// Exterior cell region
    pub region: Option<String>,
    /// Grid location of the cell. (FIXME: What is its purpose for interior cells?)
    pub location: Point2<i32>,
    /// Traits that apply to this cell
    pub traits: LinkedHashSet<CellTraits>,
    /// Colour of the ambient light within the cell
    pub ambient_light: Option<RGBAColour>,
    /// Colour of the sunlight within the cell
    pub sunlight: Option<RGBAColour>,
    /// Colour of the fog within the cell
    pub fog: Option<RGBAColour>,
    /// Density of the fog within the cell
    pub fog_density: Option<f64>,
    /// Water level within the cell
    // Note to self: Can either be an int (INTV) or float (WHGT)
    pub water_level: Option<f64>,
    /// Colour of the cell on the minimap (FIXME: Verify)
    pub map_colour: Option<RGBAColour>,
    /// Objects within this cell which are specific instances of other records.
    pub references: LinkedHashMap<CellRefId, Deletable<CellRef>>,
    /// References moved from this cell to others.
    /// Only applies to exterior cells
    #[serde(default, skip_serializing_if = "is_default")]
    pub moved_references: LinkedHashMap<CellRefId, Point2<i32>>,
}

impl Cell {
    /// Applies another Cell to this Cell
    ///
    /// This replaces the override behaviour to match that in Morrowind, where
    /// Cell records are only accompanied by new/changed cell references.
    pub fn apply(&mut self, other: &Self) {
        self.name = other.name.clone();
        self.location = other.location;
        self.traits = other.traits.clone();
        // Note: only name, location and traits are overridden when merging in OpenMW
        // All others only override if they are present (
        // See https://gitlab.com/OpenMW/openmw/-/blob/a5acc22954b07ac3dae82530c0fe2cc5728300e0/apps/openmw/mwworld/store.cpp#L685-688
        dassign_opt!(self.region, other.region);
        dassign_opt!(self.ambient_light, other.ambient_light);
        dassign_opt!(self.sunlight, other.sunlight);
        dassign_opt!(self.fog, other.fog);
        dassign_opt!(self.fog_density, other.fog_density);
        dassign_opt!(self.water_level, other.water_level);
        dassign_opt!(self.map_colour, other.map_colour);

        // Merge references
        for (id, value) in other.references.iter() {
            self.references.insert(id.clone(), value.clone());
        }
        // Merge moved references
        for (id, target) in other.moved_references.iter() {
            self.moved_references.insert(id.clone(), *target);
        }
    }

    pub fn vanilla_diff(&self, other: &Self) -> Option<Self> {
        fn vanilla_diff<V: Clone + PartialEq>(this: &Option<V>, other: &Option<V>) -> Option<V> {
            if let (Some(this_value), Some(other_value)) = (this, other) {
                if this_value != other_value {
                    Some(other_value.clone())
                } else {
                    None
                }
            } else {
                other.clone()
            }
        }

        let region = vanilla_diff(&self.region, &other.region);

        let mut ambient_light = vanilla_diff(&self.ambient_light, &other.ambient_light);
        let mut sunlight = vanilla_diff(&self.sunlight, &other.sunlight);
        let mut fog = vanilla_diff(&self.fog, &other.fog);
        let mut fog_density = vanilla_diff(&self.fog_density, &other.fog_density);

        // All are grouped together in ouput. Either we include none, or all of them
        if ambient_light.is_some() || sunlight.is_some() || fog.is_some() || fog_density.is_some() {
            ambient_light = other.ambient_light;
            sunlight = other.sunlight;
            fog = other.fog;
            fog_density = other.fog_density;
        }

        let water_level = vanilla_diff(&self.water_level, &other.water_level);
        let map_colour = vanilla_diff(&self.map_colour, &other.map_colour);
        let references = create_map_vanilla_diff(&self.references, &other.references);
        let mut moved_references = LinkedHashMap::new();
        for (id, target) in other.moved_references.iter() {
            if self.moved_references.get(id) != Some(target) {
                moved_references.insert(id.clone(), *target);
            }
        }

        // If the data has not changed, return the old one
        if other.name == self.name
            && region.is_none()
            && other.location == self.location
            && other.traits == self.traits
            && ambient_light.is_none()
            && sunlight.is_none()
            && fog.is_none()
            && fog_density.is_none()
            && water_level.is_none()
            && map_colour.is_none()
            && references.is_empty()
            && moved_references.is_empty()
        {
            None
        } else {
            Some(Self::new(
                other.name.clone(),
                region,
                other.location,
                other.traits.clone(),
                ambient_light,
                sunlight,
                fog,
                fog_density,
                water_level,
                map_colour,
                references,
                moved_references,
            ))
        }
    }
}

impl TryFrom<esplugin::Record> for RecordPair<Cell> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut name = None;
        let mut region = None;
        let mut cell_data = None;
        let mut ambi_data = None;
        let mut water_level = None;
        let mut map_colour = None;

        #[derive(Nom, PartialEq, Debug)]
        #[nom(LittleEndian)]
        struct Data {
            flags: u32,
            x: i32,
            y: i32,
        }

        #[derive(Nom, Copy, Clone)]
        #[nom(LittleEndian)]
        struct AmbiData {
            #[nom(Parse = "take_rgba_colour")]
            ambient: RGBAColour,
            #[nom(Parse = "take_rgba_colour")]
            sunlight: RGBAColour,
            #[nom(Parse = "take_rgba_colour")]
            fog: RGBAColour,
            #[nom(Parse = "le_f32")]
            fog_density: f32,
        }

        #[derive(Debug, PartialEq)]
        enum Scope {
            Cell,
            CellRef,
            MoveRef,
        }
        let mut reference_scope = Scope::Cell;
        let mut is_delete_record = false;
        // Tracks the beginning of the reference in the subrecord list
        let mut ref_subs_start = 0;

        let mut references: LinkedHashMap<CellRefId, Deletable<CellRef>> = LinkedHashMap::new();
        let mut moved_references: LinkedHashMap<CellRefId, Point2<i32>> = LinkedHashMap::new();
        let mut moved_ref: Option<CellRefId> = None;

        for (index, subrecord) in subrecords.iter().enumerate() {
            let data = subrecord.data();
            if subrecord.subrecord_type() == b"FRMR" {
                reference_scope = Scope::CellRef;
                ref_subs_start = index;
            } else if subrecord.subrecord_type() == b"MVRF" {
                reference_scope = Scope::MoveRef;
            }
            match reference_scope {
                Scope::Cell => match subrecord.subrecord_type() {
                    b"NAME" => name = Some(take_nt_str(data)?.1),
                    b"RGNN" => region = Some(take_nt_str(data)?.1),
                    b"DATA" if cell_data == None => cell_data = Some(Data::parse(data)?.1),
                    b"AMBI" => ambi_data = Some(AmbiData::parse(data)?.1),
                    b"INTV" => water_level = Some(le_i32(data)?.1 as f64),
                    b"WHGT" => water_level = Some(parse_float(le_f32(data)?.1)),
                    b"NAM5" => map_colour = Some(take_rgba_colour(data)?.1),
                    b"DELE" | b"NAM0" => (),
                    x => return Err(RecordError::UnexpectedSubrecord(*x)),
                },
                Scope::CellRef => {
                    if b"DELE" == subrecord.subrecord_type() {
                        is_delete_record = true;
                    }
                }
                Scope::MoveRef => match subrecord.subrecord_type() {
                    // Idea: What if cell references were stored outside cells, and had globally unique
                    // ids (which they do, as far as I know), with a field identifying the cell they
                    // belong to. That field could be changed to move the reference.
                    // Heirarchical groups could be used to group the references by cell, but the
                    // engine needn't use these to identify the references.
                    //
                    // Only allowed on exterior cells
                    // (maybe a good idea to split the implementations?)
                    b"MVRF" => {
                        moved_ref = Some(data.try_into()?);
                    }
                    b"CNDT" => {
                        let (x, y) = tuple((le_i32, le_i32))(data)?.1;
                        if let Some(mref) = moved_ref {
                            moved_references.insert(mref, Point2::new(x, y));
                            moved_ref = None;
                        } else {
                            return Err(RecordError::MissingSubrecord("MVRF"));
                        }
                    }
                    x => return Err(RecordError::UnexpectedSubrecord(*x)),
                },
            }
            if reference_scope == Scope::CellRef {
                // If this is the end of a cell_ref, parse the reference
                let next_new_scope = index + 1 < subrecords.len()
                    && (subrecords[index + 1].subrecord_type() == b"FRMR"
                        || subrecords[index + 1].subrecord_type() == b"MVRF");
                if next_new_scope || index == subrecords.len() - 1 {
                    let subs: &[Subrecord] = &subrecords[ref_subs_start..index + 1];
                    let id = CellRef::get_id(subs)?;
                    if is_delete_record {
                        references.insert(id, Deletable::Delete);
                    } else {
                        let pair: Pair<CellRefId, CellRef> = subs.try_into()?;
                        references.insert(pair.id, Deletable::Add(pair.value));
                    }
                    is_delete_record = false;
                }
            }
        }

        let data = cell_data.ok_or(RecordError::MissingSubrecord("DATA"))?;
        let traits: LinkedHashSet<CellTraits> =
            BitFlags::<CellTraits>::from_bits_truncate(data.flags)
                .iter()
                .collect();
        let location = Point2 {
            x: data.x,
            y: data.y,
        };
        let id = if traits.contains(&CellTraits::Interior) {
            RecordId::String(
                RecordTypeName::Cell,
                name.clone().ok_or(RecordError::MissingSubrecord("NAME"))?,
            )
        } else {
            RecordId::ExteriorCell(location)
        };

        let cell = Cell::new(
            name.unwrap_or(String::new()),
            region,
            location,
            traits,
            ambi_data.map(|data| data.ambient),
            ambi_data.map(|data| data.sunlight),
            ambi_data.map(|data| data.fog),
            ambi_data.map(|data| parse_float(data.fog_density)),
            water_level,
            map_colour,
            references,
            moved_references,
        );
        Ok(RecordPair::new(id, cell))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Cell> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let record = self.record;

        push_str_subrecord(&mut subrecords, &record.name, "NAME")?;
        let mut data = vec![];
        let mut flags = 0u32;
        for flag in record.traits {
            flags |= flag as u32;
        }
        data.extend(&(flags.to_le_bytes()));
        data.extend(&((record.location.x as i32).to_le_bytes()));
        data.extend(&((record.location.y as i32).to_le_bytes()));
        subrecords.push(Subrecord::new(*b"DATA", data, false));
        push_opt_str_subrecord(&mut subrecords, &record.region, "RGNN")?;

        if record.ambient_light.is_some()
            || record.sunlight.is_some()
            || record.fog.is_some()
            || record.fog_density.is_some()
        {
            let mut data = vec![];
            data.extend(&record.ambient_light.unwrap_or_default().to_le_bytes());
            data.extend(&record.sunlight.unwrap_or_default().to_le_bytes());
            data.extend(&record.fog.unwrap_or_default().to_le_bytes());
            data.extend(&(record.fog_density.unwrap_or(0.) as f32).to_le_bytes());
            subrecords.push(Subrecord::new(*b"AMBI", data, false));
        }

        if let Some(water_level) = record.water_level {
            subrecords.push(Subrecord::new(
                *b"WHGT",
                (water_level as f32).to_le_bytes().to_vec(),
                false,
            ));
        }
        if let Some(map_colour) = record.map_colour {
            subrecords.push(Subrecord::new(
                record_type("NAM5"),
                map_colour.to_le_bytes().to_vec(),
                false,
            ));
        }

        for (id, cell_ref) in record.references.into_iter() {
            match cell_ref {
                Deletable::Add(cell_ref) => {
                    let subs: Vec<esplugin::Subrecord> = Pair::new(id, cell_ref).try_into()?;
                    subrecords.extend(subs);
                }
                Deletable::Delete => {
                    let data: [u8; 4] = id.try_into()?;
                    subrecords.push(Subrecord::new(*b"FRMR", data.to_vec(), false));
                    // Morrowind requires the name of the object, in addition to the reference
                    // identifier.
                    // OpenMW doesn't really need it, as the object will be deleted, but
                    // it does store the cell reference, so we need to reference a valid object.
                    // The merged patch includes a special object with the identifier <delete> for
                    // this purpose.
                    push_str_subrecord(&mut subrecords, "<deleted>", "NAME")?;
                    subrecords.push(Subrecord::new(*b"DELE", 0i32.to_le_bytes().to_vec(), false));
                }
            }
        }

        // FIXME: These may be supposed to go before the corresponding reference.
        // I'm not sure openmw cares.
        for (id, target) in record.moved_references.into_iter() {
            let data: [u8; 4] = id.try_into()?;
            subrecords.push(Subrecord::new(*b"MVRF", data.to_vec(), false));

            let mut target_vec = (target.x as i32).to_le_bytes().to_vec();
            target_vec.extend((target.y as i32).to_le_bytes().to_vec());
            subrecords.push(Subrecord::new(*b"CNDT", target_vec, false));
        }
        let header = RecordHeader::new(*b"CELL", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
