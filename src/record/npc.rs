/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::ai::{AIData, Behaviour, DeltaAIData};
use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{
    parse_attributes, parse_skills, parse_stats, AttributeType, SkillType, StatType, Transport,
};
use crate::util::{
    is_default, nt_str, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    take_point, take_vector, RecordError,
};
use derive_more::{Constructor, Display};
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::{LinkedHashMap, LinkedHashSet};
use nom::number::complete::{le_u32, le_u8};
use nom::sequence::tuple;
use nom_derive::Parse;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum NPCTraits {
    Female = 0x01,
    Essential = 0x02,
    Respawn = 0x04,
    Base = 0x08,
    Autocalc = 0x10,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct NPC {
    /// In-game name for the NPC
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// NPC's Race identifier
    pub race: Option<String>,
    /// NPC's Facrtion identifier
    pub faction: Option<String>,
    /// NPC's Class identifier
    pub class: Option<String>,
    /// Hair Model
    pub hair: Option<String>,
    /// Head Model
    pub head: Option<String>,
    /// NPC's level
    pub level: u32,
    /// NPC's attributes. If not present, will be autocalculated
    pub attributes: Option<LinkedHashMap<AttributeType, i32>>,
    /// NPC's statistics. If not present, will be autocalculated
    pub stats: Option<LinkedHashMap<StatType, i32>>,
    /// NPC's skills. If not present, will be autocalculated
    pub skills: Option<LinkedHashMap<SkillType, u32>>,
    /// NPC Disposision (FIXME: Presumably base disposition?)
    pub disposition: u32,
    /// NPC Reputation
    pub reputation: u32,
    /// NPC Rank (FIXME: Within their faction?)
    pub rank: u32,
    // FIXME: What is this?
    unknown1: Option<i8>,
    unknown2: Option<i8>,
    unknown3: Option<i8>,
    /// Gold carried by the NPC
    pub gold: i32,
    /// NPC's traits
    #[serde(default, skip_serializing_if = "is_default")]
    pub traits: LinkedHashSet<NPCTraits>,
    /// The blood texture index used for this NPC
    #[serde(default, skip_serializing_if = "is_default")]
    pub blood_index: u8,
    /// FIXME: When is this script run?
    pub script: Option<String>,
    /// FIXME: What does this mean?
    pub persistent: Option<bool>,
    /// The inventory carried by the npc
    #[serde(default, skip_serializing_if = "is_default")]
    pub inventory: LinkedHashMap<String, u64>,
    /// The NPC's spells
    pub spells: LinkedHashSet<String>,
    #[delta]
    pub ai: Option<AIData>,
    #[serde(default, skip_serializing_if = "is_default")]
    #[multiset]
    pub behaviours: Vec<Behaviour>,
    #[serde(default, skip_serializing_if = "is_default")]
    #[multiset]
    pub transport: Vec<Transport>,
}

impl TryFrom<esplugin::Record> for RecordPair<NPC> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let persistent = (record.header().flags() & 0x0400) != 0;
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct LongData {
            level: i16,
            #[nom(Count = "8")]
            attributes: Vec<u8>,
            #[nom(Count = "27")]
            skills: Vec<u8>,
            unknown1: i8,
            #[nom(Count = "3")]
            stats: Vec<u16>,
            disposition: u8,
            reputation: u8,
            rank: u8,
            unknown2: i8,
            gold: i32,
        }

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct ShortData {
            level: i16,
            disposition: u8,
            reputation: u8,
            rank: u8,
            unknown1: i8,
            unknown2: i8,
            unknown3: i8,
            gold: i32,
        }

        let mut race = None;
        let mut faction = None;
        let mut class = None;
        let mut head = None;
        let mut hair = None;
        let mut traits = None;
        let mut inventory = LinkedHashMap::new();
        let mut spells = LinkedHashSet::new();
        let mut ai = None;
        let mut behaviours = vec![];
        let mut transport = vec![];
        let mut long_data = None;
        let mut short_data = None;
        let mut blood_index = 0;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"ANAM" => faction = Some(take_nt_str(data)?.1),
                b"BNAM" => head = Some(take_nt_str(data)?.1),
                b"CNAM" => class = Some(take_nt_str(data)?.1),
                b"KNAM" => hair = Some(take_nt_str(data)?.1),
                b"RNAM" => race = Some(take_nt_str(data)?.1),
                b"NPDT" => {
                    if data.len() == 52 {
                        long_data = Some(LongData::parse(data)?.1);
                    } else {
                        assert!(data.len() == 12);
                        short_data = Some(ShortData::parse(data)?.1);
                    }
                }
                b"FLAG" => {
                    let (data, flags) = le_u8(data)?;
                    traits = Some(
                        BitFlags::<NPCTraits>::from_bits_truncate(flags as u32)
                            .iter()
                            .collect(),
                    );
                    blood_index = le_u8(data)?.1 >> 2;
                }
                b"NPCO" => {
                    let (data, count) = le_u32(data)?;
                    let (_, name) = take_nt_str(data)?;
                    inventory.insert(name, count as u64);
                }
                b"NPCS" => {
                    spells.insert(take_nt_str(data)?.1);
                }
                b"CNDT" => {
                    let mut last = behaviours.last_mut();
                    match &mut last {
                        Some(Behaviour::Follow(ref mut target))
                        | Some(Behaviour::Escort(ref mut target)) => {
                            target.cell = Some(take_nt_str(data)?.1);
                        }
                        _ => {
                            return Err(RecordError::UnexpectedSubrecord(*b"CNDT"));
                        }
                    }
                }
                b"AIDT" => ai = Some(AIData::parse(data)?),
                b"AI_W" | b"AI_T" | b"AI_F" | b"AI_E" | b"AI_A" => {
                    behaviours.push(Behaviour::parse(subrecord.subrecord_type(), data)?);
                }
                b"DODT" => {
                    // Transport position
                    let (_, (position, rotation)) = tuple((take_point, take_vector))(data)?;
                    transport.push(Transport {
                        position,
                        rotation,
                        cell_name: String::new(),
                    });
                }
                b"DNAM" => {
                    // Transport cell name
                    if let Some(mut last) = transport.last_mut() {
                        last.cell_name = take_nt_str(data)?.1;
                    } else {
                        return Err(RecordError::UnexpectedSubrecord(*b"DNAM"));
                    }
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let traits = traits.ok_or(RecordError::MissingSubrecord("FLAG"))?;

        let (
            level,
            attributes,
            skills,
            unknown1,
            stats,
            disposition,
            reputation,
            rank,
            unknown2,
            gold,
            unknown3,
        ) = match (long_data, short_data) {
            (Some(data), None) => (
                data.level as u32,
                Some(parse_attributes(data.attributes.into_iter())),
                Some(parse_skills::<u8>(&data.skills)),
                Some(data.unknown1),
                Some(parse_stats::<u16>(&data.stats)),
                data.disposition as u32,
                data.reputation as u32,
                data.rank as u32,
                Some(data.unknown2),
                data.gold,
                None,
            ),
            (None, Some(data)) => (
                data.level as u32,
                None,
                None,
                Some(data.unknown1),
                None,
                data.disposition as u32,
                data.reputation as u32,
                data.rank as u32,
                Some(data.unknown2),
                data.gold,
                Some(data.unknown3),
            ),
            (None, None) => return Err(RecordError::MissingSubrecord("NPDT")),
            (Some(_), Some(_)) => unreachable!(), // Actually could occur if there are duplicate NPDT
        };
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::NPC, id),
            NPC::new(
                name,
                model,
                race,
                faction,
                class,
                hair,
                head,
                level,
                attributes,
                stats,
                skills,
                disposition,
                reputation,
                rank,
                unknown1,
                unknown2,
                unknown3,
                gold,
                traits,
                blood_index,
                script,
                if persistent { Some(persistent) } else { None },
                inventory,
                spells,
                ai,
                behaviours,
                transport,
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<NPC> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.race, "RNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.class, "CNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.faction, "ANAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.head, "BNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.hair, "KNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        let mut data = vec![];
        data.extend(&(self.record.level as i16).to_le_bytes());
        if let (Some(skills), Some(attributes), Some(stats)) = (
            self.record.skills,
            self.record.attributes,
            self.record.stats,
        ) {
            use self::AttributeType::*;
            for attribute in &[
                Strength,
                Intelligence,
                Willpower,
                Agility,
                Speed,
                Endurance,
                Personality,
                Luck,
            ] {
                data.extend(&(attributes[&attribute] as u8).to_le_bytes());
            }
            for skill_id in 0..=SkillType::max() {
                data.extend(
                    &(skills[&FromPrimitive::from_usize(skill_id).unwrap()] as u8).to_le_bytes(),
                );
            }
            data.extend(&self.record.unknown1.unwrap_or(0).to_le_bytes());
            use self::StatType::*;
            for stat in &[Health, Mana, Fatigue] {
                data.extend(&(stats[&stat] as u16).to_le_bytes());
            }
            data.extend(&(self.record.disposition as u8).to_le_bytes());
            data.extend(&(self.record.reputation as u8).to_le_bytes());
            data.extend(&(self.record.rank as u8).to_le_bytes());
            data.extend(&self.record.unknown2.unwrap_or(0i8).to_le_bytes());
        } else {
            data.extend(&(self.record.disposition as u8).to_le_bytes());
            data.extend(&(self.record.reputation as u8).to_le_bytes());
            data.extend(&(self.record.rank as u8).to_le_bytes());
            data.extend(&self.record.unknown1.unwrap_or(0i8).to_le_bytes());
            data.extend(&self.record.unknown2.unwrap_or(0i8).to_le_bytes());
            data.extend(&self.record.unknown3.unwrap_or(0i8).to_le_bytes());
        }
        data.extend(&(self.record.gold as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"NPDT", data, false));

        if !self.record.traits.is_empty() || self.record.blood_index != 0 {
            let mut flagbits = 0u32;
            for flag in self.record.traits {
                flagbits |= flag as u32;
            }
            flagbits |= (self.record.blood_index as u32) << 10;
            subrecords.push(Subrecord::new(
                *b"FLAG",
                (&flagbits.to_le_bytes()).to_vec(),
                false,
            ));
        }

        for spell in self.record.spells {
            push_str_subrecord(&mut subrecords, &spell, "NPCS")?;
        }
        for (name, count) in self.record.inventory {
            let mut data = vec![];
            data.extend((count as u32).to_le_bytes().to_vec());
            data.extend(nt_str(&name)?);
            data.resize(36, 0);
            subrecords.push(Subrecord::new(*b"NPCO", data, false));
        }
        if let Some(ai) = self.record.ai {
            subrecords.push(ai.try_into()?);
        }
        for behaviour in self.record.behaviours {
            let subs: Vec<esplugin::Subrecord> = behaviour.try_into()?;
            subrecords.extend(subs);
        }

        let flags = if let Some(true) = self.record.persistent {
            0x0400
        } else {
            0
        };

        for transport in self.record.transport {
            let _subs: Vec<Subrecord> = transport.try_into()?;
            subrecords.extend(_subs);
        }

        let header = RecordHeader::new(*b"NPC_", flags, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
