/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::ai::{AIData, Behaviour, DeltaAIData};
use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{
    parse_attributes, parse_stats, AttributeType, SpecializationType, StatType, Transport,
};
use crate::util::{
    is_default, nt_str, parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len,
    take_nt_str, take_point, take_vector, RecordError,
};
use derive_more::{Constructor, Display};
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::{LinkedHashMap, LinkedHashSet};
use nom::number::complete::{le_f32, le_u32, le_u8};
use nom::sequence::tuple;
use nom_derive::Parse;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, FromPrimitive, Hash, ToPrimitive, Serialize, Deserialize,
)]
pub enum CreatureType {
    Creature = 0,
    Daedra = 1,
    Undead = 2,
    Humanoid = 3,
}

impl Default for CreatureType {
    fn default() -> Self {
        CreatureType::Creature
    }
}

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum CreatureTraits {
    Biped = 0x0001,
    Respawn = 0x0002,
    WeaponAndShield = 0x0004,
    Base = 0x0008,
    Swims = 0x0010,
    Flies = 0x0020,
    Walks = 0x0040,
    Essential = 0x0080,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Creature {
    /// In-game name for the creature
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// Type of creature
    pub creature_type: CreatureType,
    /// Original creature that this is a modification of
    pub original: Option<String>,
    /// Creature's level
    pub level: u32,
    /// Creature's attributes
    pub attributes: LinkedHashMap<AttributeType, i32>,
    /// Creature's statistics
    pub stats: LinkedHashMap<StatType, i32>,
    /// Creature's soul value
    pub soul: i32,
    /// Creature's skills, grouped by specialization
    pub skills: LinkedHashMap<SpecializationType, i32>,
    /// FIXME: What do these attacks mean?
    /// 3 Attacks, values give min and max of... something
    pub attacks: [[i32; 2]; 3],
    /// Gold value of the creature. FIXME: What is this used for
    pub gold: i32,
    /// Creature's traits
    #[serde(default, skip_serializing_if = "is_default")]
    pub traits: LinkedHashSet<CreatureTraits>,
    /// The blood texture index used for this Creature
    #[serde(default, skip_serializing_if = "is_default")]
    pub blood_index: u8,
    /// FIXME: When is this script run?
    pub script: Option<String>,
    /// FIXME: What does this mean?
    #[serde(default, skip_serializing_if = "is_default")]
    pub persistent: bool,
    /// The inventory carried by the creature
    #[serde(default, skip_serializing_if = "is_default")]
    pub inventory: LinkedHashMap<String, u64>,
    /// The creature's spells
    #[serde(default, skip_serializing_if = "is_default")]
    pub spells: LinkedHashSet<String>,
    /// Scale of the creature
    /// FIXME: What does it mean if this is omitted?
    pub scale: Option<f64>,
    #[delta]
    pub ai: AIData,
    #[serde(default, skip_serializing_if = "is_default")]
    #[multiset]
    pub behaviours: Vec<Behaviour>,
    #[serde(default, skip_serializing_if = "is_default")]
    #[multiset]
    pub transport: Vec<Transport>,
}

impl TryFrom<esplugin::Record> for RecordPair<Creature> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();

        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut original = None;
        let persistent = (record.header().flags() & 0x0400) != 0;
        let mut creature_data = None;
        let mut traits = LinkedHashSet::new();
        let mut inventory = LinkedHashMap::new();
        let mut spells = LinkedHashSet::new();
        let mut scale = None;
        let mut behaviours = vec![];
        let mut ai = None;
        let mut transport = vec![];
        let mut blood_index = 0;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            creature_type: i32,
            level: u32,
            #[nom(Count = "8")]
            attributes: Vec<i32>,
            #[nom(Count = "3")]
            stats: Vec<i32>,
            soul: i32,
            combat: i32,
            magic: i32,
            stealth: i32,
            #[nom(Count = "6")]
            attacks: Vec<i32>,
            gold: i32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"CNAM" => original = Some(take_nt_str(data)?.1),
                b"NPDT" => {
                    creature_data = Some(Data::parse(data)?.1);
                }
                b"FLAG" => {
                    let (data, flags) = le_u8(data)?;
                    traits = BitFlags::<CreatureTraits>::from_bits_truncate(flags as u32)
                        .iter()
                        .collect();
                    blood_index = le_u8(data)?.1 >> 2;
                }
                b"NPCO" => {
                    let (data, count) = le_u32(data)?;
                    let (_, name) = take_nt_str(data)?;
                    inventory.insert(name, count as u64);
                }
                b"NPCS" => {
                    spells.insert(take_nt_str(data)?.1);
                }
                b"CNDT" => {
                    let mut last = behaviours.last_mut();
                    match &mut last {
                        Some(Behaviour::Follow(ref mut target))
                        | Some(Behaviour::Escort(ref mut target)) => {
                            target.cell = Some(take_nt_str(data)?.1);
                        }
                        _ => {
                            return Err(RecordError::UnexpectedSubrecord(*b"CNDT"));
                        }
                    }
                }
                b"AIDT" => ai = Some(AIData::parse(data)?),
                b"AI_W" | b"AI_T" | b"AI_F" | b"AI_E" | b"AI_A" => {
                    behaviours.push(Behaviour::parse(subrecord.subrecord_type(), data)?);
                }
                b"XSCL" => scale = Some(parse_float(le_f32(data)?.1)),
                b"DODT" => {
                    // Transport position
                    let (_, (position, rotation)) = tuple((take_point, take_vector))(data)?;
                    transport.push(Transport::new(
                        position.into(),
                        rotation.into(),
                        String::new(),
                    ));
                }
                b"DNAM" => {
                    // Transport cell name
                    if let Some(mut last) = transport.last_mut() {
                        last.cell_name = take_nt_str(data)?.1;
                    } else {
                        return Err(RecordError::UnexpectedSubrecord(*b"DNAM"));
                    }
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;

        let data = creature_data.ok_or(RecordError::MissingSubrecord("NPDT"))?;
        let ai = ai.ok_or(RecordError::MissingSubrecord("AIDT"))?;

        let creature_type = FromPrimitive::from_i32(data.creature_type)
            .ok_or(RecordError::InvalidCreatureType(data.creature_type))?;
        let skills = vec![
            (SpecializationType::Combat, data.combat),
            (SpecializationType::Magic, data.magic),
            (SpecializationType::Stealth, data.stealth),
        ]
        .into_iter()
        .collect();
        let attacks = [
            [data.attacks[0], data.attacks[1]],
            [data.attacks[2], data.attacks[3]],
            [data.attacks[4], data.attacks[5]],
        ];
        let creature = Creature::new(
            name,
            model,
            creature_type,
            original,
            data.level,
            parse_attributes(data.attributes.into_iter()),
            parse_stats(&data.stats[0..3]),
            data.soul,
            skills,
            attacks,
            data.gold,
            traits,
            blood_index,
            script,
            persistent,
            inventory,
            spells,
            scale,
            ai,
            behaviours,
            transport,
        );
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Creature, id),
            creature,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Creature> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.original, "CNAM")?;
        let mut data = vec![];
        data.extend(&(self.record.creature_type as i32).to_le_bytes());
        data.extend(&(self.record.level as i32).to_le_bytes());
        use self::AttributeType::*;
        for attribute in &[
            Strength,
            Intelligence,
            Willpower,
            Agility,
            Speed,
            Endurance,
            Personality,
            Luck,
        ] {
            data.extend(&(self.record.attributes[&attribute] as i32).to_le_bytes());
        }
        use self::StatType::*;
        for stat in &[Health, Mana, Fatigue] {
            data.extend(&(self.record.stats[&stat] as i32).to_le_bytes());
        }
        data.extend(&(self.record.soul as i32).to_le_bytes());
        use self::SpecializationType::*;
        for skill in &[Combat, Magic, Stealth] {
            data.extend(&(self.record.skills[skill] as i32).to_le_bytes());
        }
        for attack in self.record.attacks.iter() {
            for value in attack {
                data.extend(&(*value as i32).to_le_bytes());
            }
        }
        data.extend(&(self.record.gold as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"NPDT", data, false));

        if self.record.traits.len() > 0 || self.record.blood_index != 0 {
            let mut flagbits = 0u32;
            for flag in self.record.traits {
                flagbits |= flag as u32;
            }
            flagbits |= (self.record.blood_index as u32) << 10;
            subrecords.push(Subrecord::new(
                *b"FLAG",
                (&flagbits.to_le_bytes()).to_vec(),
                false,
            ));
        }

        if let Some(scale) = self.record.scale {
            subrecords.push(Subrecord::new(
                *b"XSCL",
                (scale as f32).to_le_bytes().to_vec(),
                false,
            ));
        }
        for spell in self.record.spells {
            push_str_subrecord(&mut subrecords, &spell, "NPCS")?;
        }
        for (name, count) in self.record.inventory {
            let mut data = vec![];
            data.extend((count as u32).to_le_bytes().to_vec());
            data.extend(nt_str(&name)?);
            data.resize(36, 0);
            subrecords.push(Subrecord::new(*b"NPCO", data, false));
        }
        subrecords.push(self.record.ai.try_into()?);
        for behaviour in self.record.behaviours {
            let subs: Vec<esplugin::Subrecord> = behaviour.try_into()?;
            subrecords.extend(subs);
        }

        for transport in self.record.transport {
            let _subs: Vec<Subrecord> = transport.try_into()?;
            subrecords.extend(_subs);
        }

        let flags = if self.record.persistent { 0x0400 } else { 0 };

        let header = RecordHeader::new(*b"CREA", flags, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
