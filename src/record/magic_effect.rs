/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::RecordPair;
use crate::types::{EffectType, RGBColour, SpellSchool};
use crate::util::{push_opt_str_subrecord, subrecords_len, take_nt_str, RecordError};
use derive_more::Display;
use enumflags2::BitFlags;
use enumflags2::_internal::RawBitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashSet;
use nom::number::complete::{le_f32, le_i32};
use nom_derive::Parse;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum EffectFlags {
    TargetSkill = 0x1,
    TargetAttribute = 0x2,
    NoDuration = 0x4,
    NoMagnitude = 0x8,
    Harmful = 0x10,
    ContinuousVfx = 0x20,
    CastSelf = 0x40,
    CastTouch = 0x80,
    CastTarget = 0x100,
    AppliedOnce = 0x1000,
    Stealth = 0x2000,
    NonRecastable = 0x4000,
    IllegalDaedra = 0x8000,
    Unreflectable = 0x10000,
    CasterLinked = 0x20000,

    AllowSpellmaking = 0x200,
    AllowEnchanting = 0x400,
    NegativeLight = 0x800,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct MagicEffect {
    school: SpellSchool,
    base_cost: f32,
    flags: LinkedHashSet<EffectFlags>,
    colour: RGBColour<u32>,
    speed: f32,
    size: f32,
    size_cap: f32,
    icon: Option<String>,
    particle_texture: Option<String>,
    cast_visual: Option<String>,
    hit_visual: Option<String>,
    bolt_visual: Option<String>,
    area_visual: Option<String>,
    desc: Option<String>,
    cast_sound: Option<String>,
    bolt_sound: Option<String>,
    hit_sound: Option<String>,
    area_sound: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<MagicEffect> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let mut icon = None;
        let mut particle_texture = None;
        let mut cast_visual = None;
        let mut bolt_visual = None;
        let mut hit_visual = None;
        let mut area_visual = None;
        let mut desc = None;
        let mut cast_sound = None;
        let mut bolt_sound = None;
        let mut hit_sound = None;
        let mut area_sound = None;
        let mut effect_type = None;
        let mut mgef_data = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            school: i32,
            #[nom(Parse = "le_f32")]
            base_cost: f32,
            flags: u32,
            red: u32,
            green: u32,
            blue: u32,
            #[nom(Parse = "le_f32")]
            size: f32,
            #[nom(Parse = "le_f32")]
            speed: f32,
            #[nom(Parse = "le_f32")]
            size_cap: f32,
        }

        for subrecord in record.subrecords() {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"PTEX" => particle_texture = Some(take_nt_str(data)?.1),
                b"CVFX" => cast_visual = Some(take_nt_str(data)?.1),
                b"BVFX" => bolt_visual = Some(take_nt_str(data)?.1),
                b"HVFX" => hit_visual = Some(take_nt_str(data)?.1),
                b"AVFX" => area_visual = Some(take_nt_str(data)?.1),
                b"DESC" => desc = Some(take_nt_str(data)?.1),
                b"CSND" => cast_sound = Some(take_nt_str(data)?.1),
                b"BSND" => bolt_sound = Some(take_nt_str(data)?.1),
                b"HSND" => hit_sound = Some(take_nt_str(data)?.1),
                b"ASND" => area_sound = Some(take_nt_str(data)?.1),
                b"INDX" => {
                    let (_, _effect_type) = le_i32(data)?;
                    effect_type = Some(
                        FromPrimitive::from_i32(_effect_type)
                            .expect(&format!("Invalid effect type {}", _effect_type)),
                    );
                }
                b"MEDT" => mgef_data = Some(Data::parse(data)?.1),
                x => return Err(RecordError::UnexpectedSubrecord(*x)),
            }
        }

        let data = mgef_data.ok_or(RecordError::MissingSubrecord("MEDT"))?;
        let effect_type: EffectType = effect_type.ok_or(RecordError::MissingSubrecord("INDX"))?;

        let school = FromPrimitive::from_i32(data.school)
            .ok_or(RecordError::SpellSchoolFromBitsError(data.school))?;

        Ok(RecordPair::new(
            effect_type.into(),
            MagicEffect {
                school,
                base_cost: data.base_cost,
                flags: BitFlags::<EffectFlags>::from_bits_truncate(data.flags)
                    .iter()
                    .collect(),
                colour: RGBColour {
                    red: data.red,
                    blue: data.blue,
                    green: data.green,
                },
                size: data.size,
                speed: data.speed,
                size_cap: data.size_cap,
                icon,
                particle_texture,
                cast_visual,
                bolt_visual,
                hit_visual,
                area_visual,
                desc,
                cast_sound,
                bolt_sound,
                hit_sound,
                area_sound,
            },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<MagicEffect> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let effect_type: EffectType = self.id.try_into()?;

        subrecords.push(Subrecord::new(
            *b"INDX",
            (effect_type as u32).to_le_bytes().to_vec(),
            false,
        ));

        let mut data = vec![];
        data.extend(&(self.record.school as i32).to_le_bytes());
        data.extend(&(self.record.base_cost as f32).to_le_bytes());
        let mut flagbits = 0;
        for flag in self.record.flags {
            flagbits |= flag.bits();
        }
        data.extend(&(flagbits as u32).to_le_bytes());
        data.extend(&(self.record.colour.red as u32).to_le_bytes());
        data.extend(&(self.record.colour.green as u32).to_le_bytes());
        data.extend(&(self.record.colour.blue as u32).to_le_bytes());
        data.extend(&(self.record.size as f32).to_le_bytes());
        data.extend(&(self.record.speed as f32).to_le_bytes());
        data.extend(&(self.record.size_cap as f32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"MEDT", data, false));

        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.particle_texture, "PTEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.cast_visual, "CVFX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.bolt_visual, "BVFX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.hit_visual, "HVFX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.area_visual, "AVFX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.desc, "DESC")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.cast_sound, "CSND")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.bolt_sound, "BSND")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.hit_sound, "HSND")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.area_sound, "ASND")?;

        let header = RecordHeader::new(*b"MGEF", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
