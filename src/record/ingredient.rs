/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{AttributeType, EffectType, SkillType};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_f32;
use nom_derive::Parse;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::convert::{TryFrom, TryInto};

#[skip_serializing_none]
#[derive(Constructor, Clone, Debug, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub struct IngredientEffect {
    /// Ingredient's effect
    pub effect: EffectType,
    /// Replated skill
    pub skill: Option<SkillType>,
    /// Related attribute
    pub attribute: Option<AttributeType>,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Ingredient {
    /// In-game name for the Ingredient
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// Icon identifier
    pub icon: Option<String>,
    /// Weight of the Ingredient
    pub weight: f64,
    /// Value of the Ingredient
    pub value: i64,
    /// Ingredient's effects for the purposes of alchemy
    /// FIXME: Certain morrowind records contain duplicates.
    /// Are they meaningful?
    #[multiset]
    pub effects: Vec<IngredientEffect>,
    /// Script (FIXME: When is the script run?)
    pub script: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Ingredient> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut icon = None;
        let mut ingredient_data = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            #[nom(Parse = "le_f32")]
            weight: f32,
            value: i32,
            #[nom(Count = "4")]
            effects: Vec<i32>,
            #[nom(Count = "4")]
            skills: Vec<i32>,
            #[nom(Count = "4")]
            attributes: Vec<i32>,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"IRDT" => {
                    ingredient_data = Some(Data::parse(data)?.1);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = ingredient_data.ok_or(RecordError::MissingSubrecord("IRDT"))?;
        let mut effects = vec![];

        // Fix broken data in records:
        // TODO: Skill and Attributes could be included in the EffectType Enum, removing the need
        // for this, however I'm not sure how this would work with the MagicEffect records, and it
        // would not translate well to C++ due to the lack of enum values.
        // This would serialize more nicely though, as effects without skill or attribute wouldn't
        // be included as part of a mapping
        use EffectType::*;
        let skill_effects: HashSet<&EffectType> = [
            AbsorbSkill,
            DrainSkill,
            DamageSkill,
            FortifySkill,
            RestoreSkill,
        ]
        .iter()
        .collect();
        let attr_effects: HashSet<&EffectType> = [
            AbsorbAttribute,
            DrainAttribute,
            DamageAttribute,
            FortifyAttribute,
            RestoreAttribute,
        ]
        .iter()
        .collect();

        for (effect, (skill, attribute)) in data
            .effects
            .into_iter()
            .zip(data.skills.into_iter().zip(data.attributes.into_iter()))
        {
            if effect >= 0 {
                let effect = effect.try_into()?;
                effects.push(IngredientEffect::new(
                    effect,
                    if skill < 0 || !skill_effects.contains(&effect) {
                        None
                    } else {
                        Some(skill.try_into()?)
                    },
                    if attribute < 0 || !attr_effects.contains(&effect) {
                        None
                    } else {
                        Some(attribute.try_into()?)
                    },
                ));
            }
        }
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Ingredient, id),
            Ingredient::new(
                name,
                model,
                icon,
                parse_float(data.weight),
                data.value as i64,
                effects,
                script,
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Ingredient> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;

        let mut data = vec![];
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        warn!("Ingredient {} had more than 4 effects after merging. The effect list has been truncated as a result", id);
        let effects = &self.record.effects[..core::cmp::min(self.record.effects.len(), 4)];

        for effect in effects {
            data.extend(&(effect.effect as i32).to_le_bytes());
        }
        for _ in effects.len()..4 {
            data.extend(&(-1 as i32).to_le_bytes());
        }
        for effect in effects {
            data.extend(&effect.skill.map(|x| x as i32).unwrap_or(-1).to_le_bytes());
        }
        for _ in effects.len()..4 {
            data.extend(&(-1 as i32).to_le_bytes());
        }
        for effect in effects {
            data.extend(
                &effect
                    .attribute
                    .map(|x| x as i32)
                    .unwrap_or(-1)
                    .to_le_bytes(),
            );
        }
        for _ in self.record.effects.len()..4 {
            data.extend(&(-1 as i32).to_le_bytes());
        }
        subrecords.push(Subrecord::new(*b"IRDT", data, false));

        let header = RecordHeader::new(*b"INGR", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
