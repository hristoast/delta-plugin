/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{nt_str, subrecords_len, take_nt_str, RecordError};
use esplugin::{RecordHeader, Subrecord};
use nom_derive::Parse;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
use std::fs;
use std::path::Path;

// Structure containing unnecessary script data that's preserved for the purpose
// of round-trip encoding
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
struct ScriptData {
    shorts: i32,
    longs: i32,
    floats: i32,
    data_size: i32,
    var_size: i32,
    variables: Vec<String>,
    compiled: Option<String>, // Base 64 string with the compiled script
}

#[skip_serializing_none]
#[derive(OverrideRecord, Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Script {
    pub script: String,
    /// Preserved information for the purpose of round-trip encoding
    preserved: Option<ScriptData>,
    // FIXME: Merge with Startup
    // pub starup: bool,
}

// TODO: Can we create deltas for scripts? It would essentially need to be a text diff for the
// script. Possible, but tricky

impl Script {
    pub fn create_script_file(&mut self, id: &RecordId, out_dir: &Path) -> Result<(), RecordError> {
        let name: &String = id.try_into()?;
        // Write script to file and replace with include tag
        fs::create_dir_all(&out_dir.join("scripts"))?;
        let script_path = out_dir
            .join("scripts")
            .join(&name)
            .with_extension("mwscript");
        fs::write(script_path.clone(), &self.script)?;
        self.script = format!("!include_str \"scripts/{}.mwscript\"", name);
        Ok(())
    }
}

impl TryFrom<esplugin::Record> for RecordPair<Script> {
    type Error = RecordError;

    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let mut variables = vec![];
        let mut script = None;
        let mut compiled = None;
        let mut header = None;
        let mut name = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            shorts: i32,
            longs: i32,
            floats: i32,
            data_size: i32,
            var_size: i32,
        }

        for subrecord in record.subrecords() {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"SCHD" => {
                    name = Some(take_nt_str(&data[0..32])?.1);
                    header = Some(Data::parse(&data[32..])?.1);
                }
                b"SCVR" => {
                    let mut data = data;
                    while !data.is_empty() {
                        let (remaining, var) = take_nt_str(&data)?;
                        variables.push(var);
                        data = remaining;
                    }
                }
                b"SCTX" => script = Some(take_nt_str(data)?.1),
                b"SCDT" => compiled = Some(base64::encode(data)),
                x => {
                    return Err(RecordError::UnexpectedSubrecord(*x));
                }
            }
        }

        let header = header.ok_or(RecordError::MissingSubrecord("SCHD"))?;
        let name = name.ok_or(RecordError::MissingSubrecord("SCHD"))?;
        let script = script.ok_or(RecordError::MissingSubrecord("SCTX"))?;

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Script, name),
            Script {
                script,
                preserved: Some(ScriptData {
                    shorts: header.shorts,
                    longs: header.longs,
                    floats: header.floats,
                    data_size: header.data_size,
                    var_size: header.var_size,
                    variables,
                    compiled,
                }),
            },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Script> {
    type Error = RecordError;

    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        let mut header = vec![];
        header.extend(id.as_bytes());
        header.resize(32, Default::default()); // Name can be at most 32 bytes long
                                               // Header also includes NumShorts, NumLongs, NumFloats, ScriptDataSize and LocalVarSize
                                               // Ignore for the moment
        if let Some(preserved) = &self.record.preserved {
            header.extend(&preserved.shorts.to_le_bytes());
            header.extend(&preserved.longs.to_le_bytes());
            header.extend(&preserved.floats.to_le_bytes());
            header.extend(&preserved.data_size.to_le_bytes());
            header.extend(&preserved.var_size.to_le_bytes());
        } else {
            header.resize(52, Default::default());
        }
        subrecords.push(Subrecord::new(*b"SCHD", header, false));
        if let Some(preserved) = self.record.preserved {
            let mut vars: Vec<u8> = vec![];
            for var in preserved.variables {
                let string = nt_str(&var)?;
                vars.extend(string);
            }
            if vars.len() > 0 {
                subrecords.push(Subrecord::new(*b"SCVR", vars, false));
            }
            if let Some(compiled) = preserved.compiled {
                let compiled = base64::decode(compiled)?;
                subrecords.push(Subrecord::new(*b"SCDT", compiled, false));
            }
        }
        let script_string = nt_str(&self.record.script)?;
        subrecords.push(Subrecord::new(*b"SCTX", script_string, false));

        let header = RecordHeader::new(*b"SCPT", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
