/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair};
use crate::types::Point2;
use crate::util::{is_default, subrecords_len, RecordError};
use base64::STANDARD;
use derive_more::Constructor;
use derive_more::Display;
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashSet;
use nom::number::complete::{le_i32, le_u32};
use nom::sequence::tuple;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
base64_serde_type!(Base64Standard, STANDARD);

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum LandscapeTraits {
    Unknown1 = 0x01,
    Unknown2 = 0x02,
    Unknown4 = 0x04,
    Unknown8 = 0x08,
}

#[derive(Display, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[serde(untagged)]
pub enum MapFile<T> {
    File(String),
    Map(T),
    None,
}

/// Landscape information
/// Ideally should be serialized as image files, however this is complex and not particularly
/// useful right now, so fields are being stored as raw bytes
#[derive(OverrideRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Landscape {
    /// Landscape traits (all unknown)
    pub traits: LinkedHashSet<LandscapeTraits>,
    /// Height Map
    #[serde(with = "Base64Standard")]
    #[serde(default, skip_serializing_if = "is_default")]
    pub height_map: Vec<u8>,
    /// Normal map
    #[serde(with = "Base64Standard")]
    #[serde(default, skip_serializing_if = "is_default")]
    pub normal_map: Vec<u8>,
    /// FIXME: This could be hidden and generated from the height map
    #[serde(with = "Base64Standard")]
    #[serde(default, skip_serializing_if = "is_default")]
    pub low_lod_height_map: Vec<u8>,
    #[serde(with = "Base64Standard")]
    #[serde(default, skip_serializing_if = "is_default")]
    pub colour_map: Vec<u8>,
    /// Land texture indices. 0 is the default texture
    /// Subtract 1 to get the corresponding LandTexture index
    /// FIXME: This doesn't seem like a good way to represent this.
    /// What exactly is it used for?
    #[serde(with = "Base64Standard")]
    #[serde(default, skip_serializing_if = "is_default")]
    pub textures: Vec<u8>,
}

impl TryFrom<esplugin::Record> for RecordPair<Landscape> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut traits: LinkedHashSet<LandscapeTraits> = LinkedHashSet::new();
        let mut normal_map = None;
        let mut height_map = None;
        let mut low_lod_height_map = None;
        let mut colour_map = None;
        let mut textures = None;

        for subrecord in subrecords {
            let data = subrecord.data();

            match subrecord.subrecord_type() {
                b"INTV" => {
                    let (_, (x, y)) = tuple((le_i32, le_i32))(data)?;
                    id = Some(Point2::new(x, y));
                }
                b"DATA" => {
                    traits = BitFlags::<LandscapeTraits>::from_bits_truncate(le_u32(data)?.1)
                        .iter()
                        .collect();
                }
                b"VNML" => normal_map = Some(data.into_iter().map(|x| *x).collect()),
                b"VHGT" => height_map = Some(data.into_iter().map(|x| *x).collect()),
                b"WNAM" => low_lod_height_map = Some(data.into_iter().map(|x| *x).collect()),
                b"VTEX" => textures = Some(data.into_iter().map(|x| *x).collect()),
                b"VCLR" => colour_map = Some(data.into_iter().map(|x| *x).collect()),
                b"DELE" => (),
                x => return Err(RecordError::UnexpectedSubrecord(*x)),
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("INTV"))?;
        Ok(RecordPair::new(
            RecordId::Landscape(id),
            Landscape::new(
                traits,
                height_map.unwrap_or(vec![]),
                normal_map.unwrap_or(vec![]),
                low_lod_height_map.unwrap_or(vec![]),
                colour_map.unwrap_or(vec![]),
                textures.unwrap_or(vec![]),
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Landscape> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: Point2<i32> = if let RecordId::Landscape(id) = self.id {
            id
        } else {
            return Err(RecordError::InvalidId(self.id.clone()));
        };

        let mut data = vec![];
        data.extend(&(id.x.to_le_bytes()));
        data.extend(&(id.y.to_le_bytes()));
        subrecords.push(Subrecord::new(*b"INTV", data, false));

        let mut data = vec![];
        let mut flags = 0u32;
        for flag in self.record.traits {
            flags |= flag as u32;
        }
        data.extend(&(flags.to_le_bytes()));
        subrecords.push(Subrecord::new(*b"DATA", data, false));
        if !self.record.normal_map.is_empty() {
            subrecords.push(Subrecord::new(*b"VNML", self.record.normal_map, false));
        }
        if !self.record.height_map.is_empty() {
            subrecords.push(Subrecord::new(*b"VHGT", self.record.height_map, false));
        }
        if !self.record.low_lod_height_map.is_empty() {
            subrecords.push(Subrecord::new(
                *b"WNAM",
                self.record.low_lod_height_map,
                false,
            ));
        }
        if !self.record.colour_map.is_empty() {
            subrecords.push(Subrecord::new(*b"VCLR", self.record.colour_map, false));
        }
        if !self.record.textures.is_empty() {
            subrecords.push(Subrecord::new(*b"VTEX", self.record.textures, false));
        }

        let header = RecordHeader::new(*b"LAND", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
