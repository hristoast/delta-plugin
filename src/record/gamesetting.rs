/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{nt_str, subrecords_len, take_nt_str, RecordError};
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::{le_f32, le_i32};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum Value {
    String(String),
    Integer(i64),
    Float(f64),
}

#[derive(OverrideRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct GameSetting {
    #[serde(flatten)]
    value: Option<Value>,
}

impl TryFrom<esplugin::Record> for RecordPair<GameSetting> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut name = None;
        let mut value = None;

        for subrecord in record.subrecords() {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => name = Some(take_nt_str(&subrecords[0].data())?.1),
                b"STRV" => value = Some(Value::String(take_nt_str(data)?.1)),
                b"INTV" => value = Some(Value::Integer(le_i32(data)?.1 as i64)),
                b"FLTV" => value = Some(Value::Float(le_f32(data)?.1.to_string().parse().unwrap())),
                x => return Err(RecordError::UnexpectedSubrecord(*x)),
            }
        }

        let name = name.ok_or(RecordError::MissingSubrecord("NAME"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::GameSetting, name),
            GameSetting { value },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<GameSetting> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));
        if let Some(value) = self.record.value {
            match value {
                Value::String(data) => {
                    subrecords.push(Subrecord::new(*b"STRV", nt_str(&data)?, false));
                }
                Value::Integer(data) => {
                    subrecords.push(Subrecord::new(
                        *b"INTV",
                        (data as u32).to_le_bytes().to_vec(),
                        false,
                    ));
                }
                Value::Float(data) => {
                    subrecords.push(Subrecord::new(
                        *b"FLTV",
                        (data as f32).to_le_bytes().to_vec(),
                        false,
                    ));
                }
            }
        }

        let header = RecordHeader::new(*b"GMST", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
