/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{AttributeType, SkillType};
use crate::util::{
    is_default, nt_str, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use hashlink::{LinkedHashMap, LinkedHashSet};
use nom::number::complete::le_i32;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(DeltaRecord, Default, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Rank {
    name: String,
    /// Required level to reach this rank, for the Faction's associated attributes
    attribute_level: Vec<i32>,
    first_skill: i32,
    second_skill: i32,
    /// Reaction with other members of the faction?
    faction_reaction: i32,
}

#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Faction {
    /// Faction Name, as displayed in game
    name: Option<String>,
    /// Faction ranks
    /// Limited to 10 ranks
    /// Ranks are identified by index,
    /// which is how they are referred to in DialogueInfo records
    ranks: LinkedHashMap<usize, Rank>,
    /// Whether the faction is shown to the player?
    #[serde(default, skip_serializing_if = "is_default")]
    hidden: bool,
    /// Two attributes associated with the faction
    attributes: LinkedHashSet<AttributeType>,
    /// Up to seven skills associated with the faction
    skills: LinkedHashSet<SkillType>,
    /// FIXME: Reactions from or to other factions?
    reactions: LinkedHashMap<String, i64>,
}

impl TryFrom<esplugin::Record> for RecordPair<Faction> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut reaction_name: Option<String> = None;
        let mut rank_names: Vec<String> = vec![];
        let mut ranks = LinkedHashMap::new();
        let mut reactions = LinkedHashMap::new();
        let mut attributes = LinkedHashSet::new();
        let mut skills = LinkedHashSet::new();
        let mut hidden = false;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"RNAM" => rank_names.push(take_nt_str(data)?.1),
                b"FADT" => {
                    let (data, attr1) = le_i32(data)?;
                    let (data, attr2) = le_i32(data)?;
                    attributes.insert(attr1.try_into()?);
                    attributes.insert(attr2.try_into()?);

                    let skip = &data[200..];
                    let mut outer_data = data;
                    for (index, rank) in rank_names.into_iter().enumerate() {
                        let (data, attr1) = le_i32(outer_data)?;
                        let (data, attr2) = le_i32(data)?;
                        let (data, first_skill) = le_i32(data)?;
                        let (data, second_skill) = le_i32(data)?;
                        let (data, faction_reaction) = le_i32(data)?;
                        outer_data = data;
                        ranks.insert(
                            index,
                            Rank {
                                name: rank,
                                attribute_level: vec![attr1, attr2],
                                first_skill,
                                second_skill,
                                faction_reaction,
                            },
                        );
                    }
                    // Skip past to the end of the 10 ranks. Some may be empty
                    outer_data = skip;
                    // We moved the value out of the vector. Clear in case another
                    // RNAM is encountered (which would be invalid, but we'll just ignore it
                    // insetead
                    rank_names = vec![];
                    for _ in 0..7 {
                        let (data, skill) = le_i32(outer_data)?;
                        outer_data = data;
                        if skill != -1 {
                            skills.insert(skill.try_into()?);
                        }
                    }
                    let (_, _hidden) = le_i32(outer_data)?;
                    hidden = _hidden == 1;
                }
                b"ANAM" => {
                    reaction_name = Some(take_nt_str(data)?.1);
                }
                b"INTV" => {
                    if let Some(reaction_name) = reaction_name {
                        reactions.insert(reaction_name, le_i32(data)?.1 as i64);
                    } else {
                        Err(RecordError::SubrecordError(format!(
                            "INTV without preceeding ANAM in FACT record"
                        )))?
                    }
                    reaction_name = None;
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;

        let faction = Faction::new(name, ranks, hidden, attributes, skills, reactions);

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Faction, id),
            faction,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Faction> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;

        // Note: It's possible that one record may remove the last rank, but a
        // different one will add a new rank after it, leading to non-consecutive ranks
        // (no matter which order they are applied)
        for i in 0..self.record.ranks.len() {
            if self.record.ranks.get(&i).is_none() {
                // Ignore extra non-consecutive ranks
                error!("Extra ranks after rank {} in Faction {} have been ignored since they are not consecutive", i, id);
                break;
            }
            push_str_subrecord(&mut subrecords, &self.record.ranks[&i].name, "RNAM")?;
        }

        let mut data = vec![];
        if self.record.attributes.is_empty() {
            return Err(RecordError::SubrecordError(format!(
                "Faction {} must have at least one related attribute!",
                &id
            )));
        }
        // FIXME: hashlink doesn't have non-mutable back or front function
        let mut attributes = self.record.attributes.clone();
        let attributes = [*attributes.front().unwrap(), *attributes.back().unwrap()];
        if self.record.attributes.len() > 2 || self.record.attributes.len() == 0 {
            error!(
                "Attributes in Faction {} has invalid length {}: {:?} will be truncated to {:?}",
                &id,
                self.record.attributes.len(),
                self.record.attributes,
                attributes,
            );
        }
        for attr in &attributes {
            data.extend(&(*attr as i32).to_le_bytes());
        }
        if self.record.attributes.len() == 1 {
            // If there's one attribute, duplicate it
            // (likely the original contained duplicate attributes, which probably means the
            // Faction isn't really using them (e.g. factions without ranks)
            data.extend(&(*self.record.attributes.front().unwrap() as i32).to_le_bytes());
        }
        for i in 0..self.record.ranks.len() {
            if self.record.ranks.get(&i).is_none() {
                // Ignore extra non-consecutive ranks
                break;
            }

            let rank = &self.record.ranks[&i];
            for attr in &rank.attribute_level {
                data.extend(&(*attr as i32).to_le_bytes());
            }
            data.extend(&(rank.first_skill as i32).to_le_bytes());
            data.extend(&(rank.second_skill as i32).to_le_bytes());
            data.extend(&(rank.faction_reaction as i32).to_le_bytes());
        }
        // Need space for 10 ranks even if there are fewer
        data.resize(208, 0);
        let skills: Vec<&SkillType> = self.record.skills.iter().collect();
        if self.record.skills.len() > 7 {
            error!(
                "Skills in Faction {} has invalid length {}: {:?} will be truncated to {:?}",
                &id,
                self.record.skills.len(),
                self.record.skills,
                &skills[..7],
            );
        }
        if skills.len() >= 7 {
            for skill in &skills[..7] {
                data.extend(&(**skill as i32).to_le_bytes());
            }
        } else {
            for skill in &skills {
                data.extend(&(**skill as i32).to_le_bytes());
            }
            // Need 7 skill entries, extras can be set to -1.
            for _ in self.record.skills.len()..7 {
                data.extend(&(-1i32).to_le_bytes());
            }
        }
        if self.record.hidden {
            data.extend(&(1i32).to_le_bytes());
        } else {
            data.extend(&(0i32).to_le_bytes());
        }
        subrecords.push(Subrecord::new(*b"FADT", data, false));

        for (faction, reaction) in self.record.reactions {
            subrecords.push(Subrecord::new(*b"ANAM", nt_str(&faction)?, false));
            subrecords.push(Subrecord::new(
                *b"INTV",
                (reaction as i32).to_le_bytes().to_vec(),
                false,
            ));
        }

        let header = RecordHeader::new(*b"FACT", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
