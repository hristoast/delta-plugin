/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::delta::Accumulator;
use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    is_default, nt_str, push_str_subrecord, record_type, subrecords_len, take_nt_str, RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashMap;
use nom::number::complete::{le_i16, le_i32, le_u8};
use parse_display::{Display, FromStr};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

pub trait LevelledListInfo {
    fn type_str(&self) -> &'static str;
    fn type_name(&self) -> RecordTypeName;
    fn list_name(&self) -> &[u8; 4];
    fn list_name_str(&self) -> &'static str;
    fn get_esplugin_flags(&self, all_levels: bool, each: bool) -> i32;
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct Creatures;
#[derive(Default, Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct Items;

#[derive(
    Constructor,
    Display,
    FromStr,
    Clone,
    PartialEq,
    Debug,
    Eq,
    Hash,
    DeserializeFromStr,
    SerializeDisplay,
)]
#[display("{id}({level})")]
/// Serializes in the form $id($level), which must be used
/// in the text plugins
pub struct LevelledListKey {
    id: String,
    level: i32,
}

// Note: When comparing output to omwllf, it should be noted that omwllf doesn't track
// when plugins remove items from a levelled list.
#[skip_serializing_none]
#[derive(DeltaRecord, Constructor, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct LevelledList<T: Clone + PartialEq + LevelledListInfo> {
    /// Levelled items
    /// Mapping of object identifier + level to the number of times they appear in the list
    pub list: LinkedHashMap<LevelledListKey, Accumulator>,
    /// Select a new item each time this list is instantiated, instead of giving several identical
    /// items (e.g. when a container has more than one instance of a levelled list).
    #[serde(default, skip_serializing_if = "is_default")]
    pub each: bool,
    /// Whether or not to calculate from all levels <= player leve, not just the closest below the
    /// player
    #[serde(default, skip_serializing_if = "is_default")]
    pub all_levels: bool,
    /// Chance that none are chosen
    #[serde(default, skip_serializing_if = "is_default")]
    pub none_chance: f64,
    /// Not serialized
    #[serde(skip)]
    pub typ: T,
}

override_fullrecord!(
    LevelledList<Items>,
    RecordType::LevelledItems,
    DeltaRecordType::DeltaLevelledItems
);
override_fullrecord!(
    LevelledList<Creatures>,
    RecordType::LevelledCreatures,
    DeltaRecordType::DeltaLevelledCreatures
);

pub type LevelledCreatures = LevelledList<Creatures>;
pub type LevelledItems = LevelledList<Items>;

pub type DeltaLevelledCreatures = DeltaLevelledList<Creatures>;
pub type DeltaLevelledItems = DeltaLevelledList<Items>;

impl LevelledListInfo for Creatures {
    fn type_str(&self) -> &'static str {
        "LEVC"
    }

    fn type_name(&self) -> RecordTypeName {
        RecordTypeName::LevelledCreatures
    }

    fn list_name(&self) -> &'static [u8; 4] {
        b"CNAM"
    }

    fn list_name_str(&self) -> &'static str {
        "CNAM"
    }

    fn get_esplugin_flags(&self, all_levels: bool, _each: bool) -> i32 {
        if all_levels {
            0x01
        } else {
            0
        }
    }
}

impl LevelledListInfo for Items {
    fn type_str(&self) -> &'static str {
        "LEVI"
    }

    fn type_name(&self) -> RecordTypeName {
        RecordTypeName::LevelledItems
    }

    fn list_name(&self) -> &'static [u8; 4] {
        b"INAM"
    }

    fn list_name_str(&self) -> &'static str {
        "INAM"
    }

    fn get_esplugin_flags(&self, all_levels: bool, each: bool) -> i32 {
        let mut flags = 0;
        if each {
            flags |= 0x01;
        }
        if all_levels {
            flags |= 0x02;
        }
        flags
    }
}

fn from_generic<T: 'static + LevelledListInfo + Default + PartialEq + Clone>(
    record: esplugin::Record,
) -> Result<(RecordId, LevelledList<T>), RecordError> {
    let subrecords = record.subrecords();
    let mut id = None;
    let mut none_chance = 0f64;
    let mut all_levels = false;
    let mut each = false;
    let mut list: LinkedHashMap<LevelledListKey, Accumulator> = LinkedHashMap::new();
    let mut last: Option<String> = None;

    let typ = T::default();

    for subrecord in subrecords {
        let data = subrecord.data();
        match subrecord.subrecord_type() {
            b"NAME" => id = Some(take_nt_str(data)?.1),
            b"NNAM" => {
                none_chance = (le_u8(data)?.1 as f64) / 100.;
            }
            b"DATA" => {
                let flags = le_i32(data)?.1;
                if (flags & 0x02) != 0 {
                    all_levels = true;
                } else if (flags & 0x01) != 0 && typ.type_str() == "LEVC" {
                    all_levels = true;
                }
                if (flags & 0x01) != 0 && typ.type_str() == "LEVI" {
                    each = true;
                }
            }
            b"INTV" => {
                let key = last.ok_or(RecordError::MissingSubrecord(&typ.list_name_str()))?;
                let level = le_i16(data)?.1 as i32;
                if let Some(old) = list.get_mut(&LevelledListKey::new(key.clone(), level)) {
                    old.add(1);
                } else {
                    list.insert(LevelledListKey::new(key, level), Accumulator::new(1));
                }
                last = None;
            }
            b"DELE" | b"INDX" => (),
            x if x == typ.list_name() => {
                let id = take_nt_str(data)?.1;
                last = Some(id);
            }
            x => Err(RecordError::UnexpectedSubrecord(*x))?,
        }
    }

    let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
    Ok((
        RecordId::String(typ.type_name(), id),
        LevelledList::new(list, each, all_levels, none_chance, typ),
    ))
}

impl TryFrom<esplugin::Record> for RecordPair<LevelledList<Items>> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let (id, record) = from_generic(record)?;
        Ok(RecordPair::new(id, record))
    }
}

impl TryFrom<esplugin::Record> for RecordPair<LevelledList<Creatures>> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let (id, record) = from_generic(record)?;
        Ok(RecordPair::new(id, record))
    }
}

impl<T: LevelledListInfo + PartialEq + Clone> LevelledList<T> {
    fn into_generic(self, id: String) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];

        push_str_subrecord(&mut subrecords, &id, "NAME")?;

        subrecords.push(Subrecord::new(
            *b"DATA",
            self.typ
                .get_esplugin_flags(self.all_levels, self.each)
                .to_le_bytes()
                .to_vec(),
            false,
        ));

        subrecords.push(Subrecord::new(
            *b"NNAM",
            ((self.none_chance * 100.) as u8).to_le_bytes().to_vec(),
            false,
        ));

        let mut list_subrecords = vec![];
        for (key, item) in self.list {
            for _ in 0..item.get() {
                list_subrecords.push(Subrecord::new(
                    *self.typ.list_name(),
                    nt_str(&key.id)?,
                    false,
                ));
                list_subrecords.push(Subrecord::new(
                    *b"INTV",
                    (key.level as i16).to_le_bytes().to_vec(),
                    false,
                ));
            }
        }
        // Since we need to count all the entries, as some are repeated, it's easier to just count
        // the result, and divide it by two, as there are two subrecords per list item
        subrecords.push(Subrecord::new(
            *b"INDX",
            ((list_subrecords.len() / 2) as i32).to_le_bytes().to_vec(),
            false,
        ));
        subrecords.extend(list_subrecords);

        let header = RecordHeader::new(
            record_type(self.typ.type_str()),
            0,
            None,
            subrecords_len(&subrecords),
        );

        Ok(esplugin::Record::new(header, subrecords))
    }
}

impl TryInto<esplugin::Record> for RecordPair<LevelledList<Items>> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        Ok(self.record.into_generic(self.id.try_into()?)?)
    }
}

impl TryInto<esplugin::Record> for RecordPair<LevelledList<Creatures>> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        Ok(self.record.into_generic(self.id.try_into()?)?)
    }
}
