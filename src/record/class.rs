/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{AttributeType, SkillType, SpecializationType};
use crate::util::{nt_str, push_opt_str_subrecord, subrecords_len, take_nt_str, RecordError};
use derive_more::{Constructor, Display};
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashSet;
use nom_derive::Parse;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum AutoCalcFlags {
    Weapon = 0x00001,
    Armor = 0x00002,
    Clothing = 0x00004,
    Books = 0x00008,
    Ingredient = 0x00010,
    Picks = 0x00020,
    Probes = 0x00040,
    Lights = 0x00080,
    Apparatus = 0x00100,
    Repair = 0x00200,
    Misc = 0x00400,
    Spells = 0x00800,
    MagicItems = 0x01000,
    Potions = 0x02000,
    Training = 0x04000,
    Spellmaking = 0x08000,
    Enchanting = 0x10000,
    RepairItem = 0x20000,
}

/// Character class information
///
/// TODO: Separating PC and NPC classes may be useful, since fields such as name and desc should
/// be mandatory for playable classes, while flags seems to be NPC-only.
#[skip_serializing_none]
#[derive(Constructor, DeltaRecord, FullRecord, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Class {
    /// In-game displayed name of the class.
    ///
    /// Optional for non-playable classes
    name: Option<String>,
    /// Class description, shown during character creation
    desc: Option<String>,
    /// Favoured Attributes of the class, which receive 10 extra points at the start
    attributes: LinkedHashSet<AttributeType>,
    /// Class Specialization
    /// Gives a 5 point bonus to the starting level of each skill matching specialization
    specialization: SpecializationType,
    /// Each major skill starts at level 30.
    /// Major skills contribute towards advancing your level when you increase them.
    /// Only 75% of the normal experience is requied to advance a major skill.
    major_skills: LinkedHashSet<SkillType>,
    /// Each minor skill starts at level 15.
    /// Minor skills contribute towards advancing your level when you increase them.
    /// Any skills not considered major or minor cost 125% experience to increase.
    minor_skills: LinkedHashSet<SkillType>,
    /// Whether or not the class is playable
    playable: bool,
    /// FIXME: NPC services provided to the player?
    flags: LinkedHashSet<AutoCalcFlags>,
}

impl TryFrom<esplugin::Record> for RecordPair<Class> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<RecordPair<Class>, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut desc = None;
        let mut class_data = None;

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            attr1: i32,
            attr2: i32,
            spec: i32,
            #[nom(Count = "10")]
            skills: Vec<i32>,
            playable: i32,
            flags: u32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"CLDT" => class_data = Some(Data::parse(data)?.1),
                b"DESC" => desc = Some(take_nt_str(data)?.1),
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = class_data.ok_or(RecordError::MissingSubrecord("CLDT"))?;

        let mut attributes = LinkedHashSet::new();
        let mut minor_skills = LinkedHashSet::new();
        let mut major_skills = LinkedHashSet::new();
        attributes.insert(data.attr1.try_into()?);
        attributes.insert(data.attr2.try_into()?);

        let specialization =
            FromPrimitive::from_i32(data.spec).ok_or(RecordError::InvalidSpecId(data.spec))?;

        for i in 0..5 {
            let minor = data.skills[2 * i];
            let major = data.skills[2 * i + 1];
            minor_skills
                .insert(FromPrimitive::from_i32(minor).ok_or(RecordError::InvalidSkillId(minor))?);
            major_skills
                .insert(FromPrimitive::from_i32(major).ok_or(RecordError::InvalidSkillId(major))?);
        }
        let playable = data.playable == 0x0001;

        let flags = BitFlags::<AutoCalcFlags>::from_bits_truncate(data.flags)
            .iter()
            .collect();

        let class = Class::new(
            name,
            desc,
            attributes,
            specialization,
            major_skills,
            minor_skills,
            playable,
            flags,
        );

        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Class, id),
            class,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Class> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));

        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;

        let mut data = vec![];
        for attribute in self.record.attributes {
            data.extend(&(attribute as i32).to_le_bytes());
        }
        data.extend(&(self.record.specialization as i32).to_le_bytes());
        for (minor, major) in self
            .record
            .minor_skills
            .into_iter()
            .zip(self.record.major_skills)
        {
            data.extend(&(minor as i32).to_le_bytes());
            data.extend(&(major as i32).to_le_bytes());
        }
        if self.record.playable {
            data.extend(&(0x0001 as u32).to_le_bytes());
        } else {
            data.extend(&(0u32).to_le_bytes());
        }
        let mut flagbits = 0;
        for flag in self.record.flags {
            flagbits |= flag as u32;
        }
        data.extend(&(flagbits as u32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"CLDT", data, false));

        push_opt_str_subrecord(&mut subrecords, &self.record.desc, "DESC")?;

        let header = RecordHeader::new(*b"CLAS", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
