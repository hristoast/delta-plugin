/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::dialogue::Dialogue;
use crate::record::header::Header;
use crate::record::{MetaRecord, RecordId, RecordPair, RecordType};
use anyhow::{Context, Error, Result};
use esplugin::{GameId, PluginEntry};
use hashlink::LinkedHashMap;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::convert::{TryFrom, TryInto};

thread_local! {
    pub static PLUGINS: RefCell<Vec<String>> = RefCell::new(vec![]);
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Plugin {
    #[serde(flatten)]
    pub header: Header,
    pub records: LinkedHashMap<RecordId, MetaRecord>,
    #[serde(skip)]
    pub file: std::path::PathBuf,
}

impl Plugin {
    pub fn header(&self) -> &Header {
        &self.header
    }

    pub fn blank(file: std::path::PathBuf) -> Self {
        Plugin {
            header: Header::default(),
            records: LinkedHashMap::new(),
            file,
        }
    }
}

impl TryFrom<esplugin::Plugin> for Plugin {
    type Error = Error;

    fn try_from(plugin: esplugin::Plugin) -> Result<Plugin> {
        let file = plugin.path().to_path_buf();
        let header = plugin.get_header_record().try_into()?;
        let mut records: LinkedHashMap<RecordId, MetaRecord> = LinkedHashMap::new();
        let mut iter = plugin.into_entries().into_iter().peekable();
        while let Some(entry) = iter.next() {
            if let esplugin::PluginEntry::Record(record) = entry {
                // Handle DIAL + INFO records. They need to be collected
                // and then converted together as a vector into a single Dialogue structure
                if b"DIAL" == &record.header_type() {
                    let mut dialogue_records: Vec<esplugin::Record> = vec![record];
                    while let Some(esplugin::PluginEntry::Record(record)) = &iter.peek() {
                        if b"INFO" == &record.header_type() {
                            // Should always succeed
                            if let Some(esplugin::PluginEntry::Record(record)) = iter.next() {
                                dialogue_records.push(record);
                            }
                        } else {
                            break;
                        }
                    }
                    let record: RecordPair<Dialogue> = dialogue_records.try_into()?;
                    records.insert(record.id, record.record.into());
                } else {
                    let header_type = record.header_type();
                    trace!(
                        "{}: Deserializing record of type {}",
                        file.file_name().unwrap().to_string_lossy(),
                        String::from_utf8_lossy(&header_type)
                    );
                    // FIXME: It would be useful to have the record identifier, if possible
                    let record: Result<RecordPair<MetaRecord>> =
                        record.try_into().context(format!(
                            "Skipping record of type {} in plugin {}",
                            String::from_utf8_lossy(&header_type),
                            file.display(),
                        ));
                    match record {
                        Ok(record) => {
                            records.insert(record.id, record.record);
                        }
                        Err(err) => {
                            error!("{:?}", err);
                        }
                    }
                }
            }
        }

        Ok(Plugin {
            header,
            records,
            file,
        })
    }
}

impl TryInto<esplugin::Plugin> for Plugin {
    type Error = Error;
    fn try_into(self) -> Result<esplugin::Plugin> {
        // Set master context information so that cell refs can serialize themselves properly
        crate::plugin::PLUGINS.with(|v| {
            let mut v = v.borrow_mut();
            v.clear();
            v.push(
                self.file
                    .file_stem()
                    .unwrap()
                    .to_os_string()
                    .into_string()
                    .unwrap()
                    .to_lowercase(),
            );
        });
        self.header.push_masters_context();
        crate::plugin::PLUGINS.with(|v| trace!("Plugins in context: {:?}", v.borrow()));

        let mut header_with_size = self.header.clone();
        header_with_size.set_num_records(
            self.records
                .iter()
                .map(|(_, record)| {
                    if let MetaRecord::Record(RecordType::Dialogue(record)) = record {
                        // Add DialogueInfo records to count
                        1 + record.info.len()
                    } else {
                        1
                    }
                })
                .sum(),
        );
        let header = header_with_size.try_into()?;

        let mut entries = vec![];

        for (id, record) in self.records.into_iter() {
            // If script is missing the script contents, try to load it from a file instead
            if let MetaRecord::Record(RecordType::Dialogue(dialogue)) = record {
                // Dialogue objects serialize into a DIAL record followed by a list of INFO records
                let records: Vec<esplugin::Record> = RecordPair::new(id, dialogue).try_into()?;
                entries.extend(records.into_iter().map(|x| PluginEntry::Record(x)));
            } else {
                trace!(
                    "{}: Serializing record {}",
                    self.file.file_name().unwrap().to_string_lossy(),
                    id
                );
                entries.push(PluginEntry::Record(RecordPair::new(id, record).try_into()?));
            };
        }

        Ok(esplugin::Plugin::new_with_contents(
            GameId::Morrowind,
            &self.file,
            header,
            entries,
        ))
    }
}
