/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use anyhow::{Context, Result};
use std::borrow::{Borrow, Cow};
use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::fs;
use std::path::Path;

use crate::plugin::Plugin;
use crate::record::header::Master;
use crate::record::{MetaRecord, RecordId, RecordType, RecordTypeName};
use crate::version::get_version;
use hashlink::LinkedHashMap;
use openmw_cfg::{config_path, find_file, get_config, get_plugins};
use rayon::prelude::*;
use std::fs::File;
use std::io::{BufReader, BufWriter};
use std::path::PathBuf;

pub fn load_all(plugin_paths: &[PathBuf], strip_cells: bool) -> HashMap<String, Result<Plugin>> {
    let plugins: Vec<(String, Result<Plugin>)> = plugin_paths
        .par_iter()
        .map(|plugin_path| {
            let basename = plugin_path.as_path().file_name().unwrap().to_str().unwrap();
            debug!("Parsing {}...", plugin_path.display());
            let plugin = parse_file(&plugin_path, strip_cells)
                .context(format!("While parsing {}", &plugin_path.display()));
            if let Err(ref err) = plugin {
                error!("{:?}", err);
            }

            (basename.to_string().to_lowercase(), plugin)
        })
        .collect();
    plugins.into_iter().collect()
}

// TODO: Should be recursive and enforce topological ordering
pub fn load_masters(plugin: &Plugin) -> Result<Vec<Plugin>> {
    let ini = get_config()?;
    let mut plugins = vec![];
    // FIXME: Use load_all to benefit from concurrency
    // Issue: find_file may not return a file that exactly matches master.name,
    // so we can't necessarily use the master list to sort the masters
    for master in &plugin.header().masters {
        let path = find_file(&ini, &master.name)?;
        debug!("Parsing {}...", path.display());
        let plugin = parse_file(&path, false)?;
        plugins.push(plugin);
    }
    Ok(plugins)
}

pub fn create_master_record_map<'a>(
    masters: &[&'a Plugin],
) -> HashMap<&'a RecordId, Cow<'a, RecordType>> {
    let mut id_map: HashMap<&RecordId, Cow<RecordType>> = HashMap::new();
    // Store most recent version of each master record in the map.

    for master in masters.iter() {
        for (id, record) in master.records.iter() {
            // Handle deleted records
            match record {
                MetaRecord::Unknown => (),
                MetaRecord::Delete => {
                    if id_map.contains_key(&id) {
                        id_map.remove(&id);
                    }
                }
                // For CELL and DIAL/INFO records, master records are
                // created by combining the versions in all the masters, not
                // overriding them
                MetaRecord::Record(RecordType::Dialogue(dialogue)) if id_map.contains_key(&id) => {
                    if let Some(cow) = id_map.get_mut(&id) {
                        if let RecordType::Dialogue(ref mut old) = cow.to_mut() {
                            old.apply(dialogue);
                        } else {
                            error!("id: {} Old: {:?}, new: {:?}", id, cow, dialogue);
                            unreachable!()
                        }
                    }
                }
                MetaRecord::Record(RecordType::Cell(cell)) if id_map.contains_key(&id) => {
                    if let Some(cow) = id_map.get_mut(&id) {
                        if let RecordType::Cell(ref mut old) = cow.to_mut() {
                            old.apply(cell);
                        } else {
                            error!("id: {} Old: {:?}, new: {:?}", id, cow, cell);
                            unreachable!()
                        }
                    }
                }
                MetaRecord::Record(record) => {
                    id_map.insert(id, Cow::Borrowed(record));
                }
                MetaRecord::Delta(_) => {
                    // FIXME: Masters should be allowed to contain delta records
                    unimplemented!()
                }
            }
        }
    }
    id_map
}

pub fn create_delta_from_masters(
    plugin: Cow<Plugin>,
    plugin_map: &HashMap<String, Plugin>,
    plugin_name: &str,
) -> Result<Plugin> {
    debug!("Creating Delta for {}...", plugin_name);
    let mut masters: Vec<&Plugin> = vec![];
    for master in &plugin.header.masters {
        if let Some(plugin) = plugin_map.get(&master.name.to_lowercase()) {
            masters.push(plugin);
        } else {
            return Err(anyhow!(
                "Master {} for plugin {} is not in the load order",
                &master.name,
                &plugin.file.display(),
            ));
        }
    }

    // Create delta plugin for plugin
    Ok(create_delta(plugin, &masters))
}

// TODO: Delta plugins should, when constructed from this function, contain a hash of the file they
// were produced from (and all masters). If the file and masters match, don't re-create the delta.
pub fn create_merged(
    output_file: &Path,
    incremental: bool,
    skip_cells: bool,
    track_changes: bool,
) -> Result<Plugin> {
    let config_path = config_path();
    let ini = get_config().context("Failed to read openmw.cfg")?;
    let mut merged = Plugin::blank(output_file.to_path_buf());
    // Dummy record for deleted cell refs to reference.
    merged.records.insert(
        RecordId::String(RecordTypeName::Activator, "<deleted>".to_string()),
        crate::record::activator::Activator::new(None, None, None).into(),
    );
    let out_dir = dirs::cache_dir().unwrap().join("delta_plugin");
    if incremental {
        fs::create_dir_all(&out_dir).context("Failed to create incremental directories")?;
    }
    // Ordered list of plugins
    let mut plugin_names = vec![];

    debug!("Parsing plugins from {}", config_path.display());
    let paths = get_plugins(&ini).context(format!(
        "While loading plugins from {}",
        &config_path.display()
    ))?;

    // Ignore plugins which we can't read
    let paths: Vec<PathBuf> = paths
        .into_iter()
        .filter(|path| {
            if let Some(Some(ext)) = path.extension().map(|x| x.to_str()) {
                match ext.to_ascii_lowercase().as_str() {
                    // Supported Plugin Types
                    "omwaddon" | "esp" | "esm" | "omwgame" => true,
                    // Deliberately omitted Plugin Types
                    // omwscripts files don't need merging
                    "omwscripts" => false,
                    _ => {
                        warn!("Unrecognized plugin type for plugin {}", &path.display());
                        false
                    }
                }
            } else {
                warn!("Unrecognized plugin type for plugin {}", &path.display());
                false
            }
        })
        .collect();

    // Load plugins and take note of any which failed to load
    // The errors will be displayed during loading, and again
    // after the merged plugin is created
    let mut errors = vec![];
    let mut plugin_map = HashMap::new();
    for (name, plugin) in load_all(&paths, skip_cells) {
        match plugin {
            Ok(plugin) => {
                plugin_map.insert(name, plugin);
            }
            Err(error) => {
                errors.push(error);
            }
        }
    }

    let mut masters = vec![];
    for plugin_path in paths {
        let basename = plugin_path.as_path().file_name().unwrap().to_str().unwrap();
        match basename {
            "OMWLLF.omwaddon" => (),
            _ if basename == output_file.file_name().unwrap().to_str().unwrap() => (),
            _ => {
                let plugin_string = basename.to_string();
                // If plugin failed to load it will be in the path list, but not in the map
                if let Some(plugin) = &plugin_map.get(&plugin_string.to_lowercase()) {
                    let master =
                        Master::new(plugin).context(format!("In plugin {}", &plugin_string))?;
                    masters.push(master);

                    plugin_names.push(plugin_string.to_lowercase());
                }
            }
        }
    }

    let delta_plugins: Result<Vec<Plugin>> = plugin_names
        .par_iter()
        .map(|plugin_name| {
            match plugin_name.to_lowercase().as_str() {
                // We don't create deltas from the base games, as Morrowind doesn't depend on anything,
                // and Bloodmoon makes some changes to records using tribunal's version as a basis despite
                // not having it as a master.
                "morrowind.esm" | "bloodmoon.esm" | "tribunal.esm" => {
                    Ok(plugin_map.get(plugin_name).unwrap().clone())
                }
                _ => {
                    let plugin = plugin_map.get(plugin_name).unwrap();

                    // Parse delta plugin from cache if it already exists
                    let cache_path = out_dir.join(plugin_name).with_extension(".yaml");
                    let path = cache_path.display().to_string();
                    let delta = if incremental && cache_path.exists() {
                        let file = File::open(cache_path)?;
                        let mut reader = BufReader::new(file);
                        let plugin: Plugin = serde_yaml::from_reader(&mut reader)
                            .context(format!("In file {}", &path))?;
                        if plugin.header.transpiler_version == Some(get_version()) {
                            plugin
                        } else {
                            create_delta_from_masters(Cow::Owned(plugin), &plugin_map, plugin_name)?
                        }
                    } else {
                        create_delta_from_masters(Cow::Borrowed(plugin), &plugin_map, plugin_name)?
                    };

                    // Write delta plugin to cache
                    // Only do this in incremental mode
                    if incremental {
                        let file = out_dir.join(&plugin_name).with_extension("yaml");
                        debug!("Writing {}...", file.display());
                        let file = File::create(file)?;
                        let writer = BufWriter::new(file);
                        serde_yaml::to_writer(writer, &delta)?;
                    }

                    Ok(delta)
                }
            }
        })
        .collect();
    let delta_plugins: Vec<Plugin> =
        delta_plugins.context("Failed to create diffs from loaded plugins")?;

    // Create up to date map of records which we can use to determine which records
    // in the merged plugin are identical to their masters.
    let master_records = create_master_record_map(
        &plugin_names
            .iter()
            .map(|name| plugin_map.get(name).unwrap())
            .collect::<Vec<_>>(),
    );

    let mut modified: LinkedHashMap<RecordId, Vec<(PathBuf, &str)>> = LinkedHashMap::new();

    let mut record_map: LinkedHashMap<RecordId, RecordType> = LinkedHashMap::new();

    // TODO: Create merged by combining delta records, then run apply_delta on the merged plugin
    // This would allow the creation of a delta yaml file, which could aid in debugging merged
    // plugins, and might be a simpler implementation.
    // Merged yaml could potentially include comments denoting which plugins supplied which fields
    // (sadly not yet supported by yaml-rust)
    for plugin in delta_plugins {
        debug!(
            "Applying Delta for {}...",
            plugin.file.as_path().file_name().unwrap().to_str().unwrap()
        );
        for (id, record) in plugin.records.into_iter() {
            match record {
                MetaRecord::Delta(delta) => {
                    if let Some(master) = record_map.get_mut(&id) {
                        master
                            .get_full_record_mut()
                            .apply_patch(MetaRecord::Delta(delta));
                        if track_changes {
                            if let Some(plugins) = modified.get_mut(&id) {
                                plugins.push((plugin.file.clone(), "Modified"));
                            } else {
                                modified.insert(id, vec![(plugin.file.clone(), "Modified")]);
                            }
                        }
                    } else {
                        warn!("Encountered Delta record when the masters have no record with the id! {:?}", id);
                    }
                }
                MetaRecord::Record(inner) => {
                    if track_changes {
                        if let Some(plugins) = modified.get_mut(&id) {
                            plugins.push((plugin.file.clone(), "Created"));
                        } else {
                            modified.insert(id.clone(), vec![(plugin.file.clone(), "Created")]);
                        }
                    }
                    match inner {
                        // For CELL and DIAL/INFO records, master records are
                        // created by combining the versions in all the masters, not
                        // overriding them
                        RecordType::Dialogue(dialogue) if record_map.contains_key(&id) => {
                            if let Some(RecordType::Dialogue(old)) = record_map.get_mut(&id) {
                                old.apply(&dialogue);
                            } else {
                                unreachable!()
                            }
                        }
                        RecordType::Cell(cell) if record_map.contains_key(&id) => {
                            if let Some(RecordType::Cell(old)) = record_map.get_mut(&id) {
                                old.apply(&cell);
                            } else {
                                unreachable!()
                            }
                        }
                        record => {
                            record_map.insert(id, record);
                        }
                    }
                }
                MetaRecord::Delete => {
                    if record_map.remove(&id).is_none() {
                        warn!(
                            "Encountered delete record {:?} with no record to delete!",
                            id
                        );
                    }
                    if track_changes {
                        if let Some(plugins) = modified.get_mut(&id) {
                            plugins.push((plugin.file.clone(), "Deleted"));
                        } else {
                            modified.insert(id, vec![(plugin.file.clone(), "Deleted")]);
                        }
                    }
                }
                MetaRecord::Unknown => (),
            }
        }
    }

    // Add records which differ from their master versions
    for (id, record) in record_map.into_iter() {
        // Since ordering is not often important and LinkedHashMaps fail equality
        // tests if ordering is different, the most effective way to determine
        // if a record differs from its master is to create a delta record and see if it's
        // non-empty

        let fullrecord = record.get_full_record();
        if let Some(MetaRecord::Delta(patch)) = fullrecord.get_patch(&master_records[&id]) {
            if !patch.is_empty() {
                if let Some(record) = diff_vanilla(&id, Cow::Owned(record), &master_records) {
                    if track_changes {
                        let strings: Vec<String> = modified
                            .get(&id)
                            .unwrap()
                            .iter()
                            .map(|(path, change)| {
                                format!(
                                    "{} ({})",
                                    path.file_name().unwrap().to_string_lossy(),
                                    change
                                )
                            })
                            .collect();
                        info!(
                            "Added merged record \"{}\" to merged plugin: {}",
                            &id,
                            strings.join(", ")
                        );
                    }
                    merged
                        .records
                        .insert(id, MetaRecord::Record(record.into_owned()));
                }
            }
        }
    }

    // Add all plugins as masters
    // TODO: only include masters which implement the records in the merged plugins
    // Note that this must include all such masters, not just the last one in the load order
    merged.header.masters.extend(masters);
    merged.header.desc = "Auto-generated merged plugin".to_string();
    merged.header.author = "delta_plugin".to_string();
    if errors.len() > 0 {
        error!("The Following Errors occured when parsing plugins!");
        for error in errors {
            error!("{:?}", error);
        }
    }
    Ok(merged)
}

fn diff_vanilla<'a>(
    id: &RecordId,
    record: Cow<'a, RecordType>,
    master_records: &HashMap<&RecordId, Cow<RecordType>>,
) -> Option<Cow<'a, RecordType>> {
    match &*record {
        // Cells and Dialogue records support omitting unchanged nested
        // CellRefs and DialogueInfos respectively.
        // We create a "vanilla diff" of them, to avoid including unnecessary
        // information
        RecordType::Cell(cell) => {
            if let RecordType::Cell(master) = &*master_records[id] {
                if let Some(diffed) = master.vanilla_diff(&cell) {
                    Some(Cow::Owned(RecordType::Cell(diffed)))
                } else {
                    None
                }
            } else {
                unreachable!();
            }
        }
        RecordType::Dialogue(dial) => {
            if let RecordType::Dialogue(master) = &*master_records[id] {
                if let Some(diffed) = master.vanilla_diff(&dial) {
                    Some(Cow::Owned(RecordType::Dialogue(diffed)))
                } else {
                    None
                }
            } else {
                unreachable!();
            }
        }
        _ => Some(record),
    }
}

/// Applies a plugin containing delta records to its masters, producing a plugin without any delta
/// records
///
/// Note that this is relatively slow, as it avoids mutating either the original plugin or its
/// masters, instead creating a completely new plugin structure.
pub fn apply_delta(plugin: Plugin, masters: &[&Plugin]) -> Plugin {
    let mut merged = Plugin::blank(plugin.file.clone());
    merged.header = plugin.header.clone();

    let mut id_map = create_master_record_map(masters);

    for (id, record) in plugin.records {
        match record {
            MetaRecord::Delta(_) => {
                if let Some(master) = id_map.get_mut(&id) {
                    let mut new = master.clone().into_owned();
                    new.get_full_record_mut().apply_patch(record);
                    if let Some(new) = diff_vanilla(&id, Cow::Owned(new), &id_map) {
                        merged
                            .records
                            .insert(id.clone(), MetaRecord::Record(new.into_owned()));
                    }
                } else {
                    panic!(
                        "Encountered Delta record when the masters have no record with that id! {:?}",
                        &id
                    );
                }
            }
            MetaRecord::Record(_) | MetaRecord::Delete => {
                merged.records.insert(id.clone(), record);
            }
            MetaRecord::Unknown => (),
        }
    }

    merged
}

pub fn create_delta(plugin: Cow<Plugin>, masters: &[&Plugin]) -> Plugin {
    let mut new_plugin = Plugin::blank(plugin.file.clone());
    new_plugin.header = plugin.header.clone();
    let id_map = create_master_record_map(masters);

    let mut deleted = HashSet::new();
    for (id, record) in plugin.records.iter() {
        if let MetaRecord::Delete = record {
            deleted.insert(id.clone());
        }
    }

    fn handle_record<'a>(
        id: Cow<RecordId>,
        record: Cow<RecordType>,
        deleted: &HashSet<RecordId>,
        new_plugin: &mut Plugin,
        id_map: &HashMap<&'a RecordId, Cow<'a, RecordType>>,
    ) {
        // FIXME: If the plugin also contains a delete record of this type,
        // Don't use the master

        // Find record matching identifier in masters
        // If records are different, create delta between master record and this record.
        let fullrecord = record.get_full_record();
        if deleted.contains(&id) {
            // Previously deleted. Ignore any existing record
            // in the masters with this id
            new_plugin
                .records
                .insert(id.into_owned(), MetaRecord::Record(record.into_owned()));
        } else if let Some(master) = id_map.get(&id.borrow()) {
            if let Some(patch) = fullrecord.get_patch(master) {
                new_plugin.records.insert(id.into_owned(), patch);
            }
        } else {
            new_plugin
                .records
                .insert(id.into_owned(), MetaRecord::Record(record.into_owned()));
        }
    }

    match plugin {
        Cow::Owned(plugin) => {
            for (id, record) in plugin.records {
                match record {
                    MetaRecord::Record(inner) => {
                        handle_record(
                            Cow::Owned(id),
                            Cow::Owned(inner),
                            &deleted,
                            &mut new_plugin,
                            &id_map,
                        );
                    }
                    MetaRecord::Delta(_) | MetaRecord::Delete => {
                        new_plugin.records.insert(id.clone(), record);
                    }
                    MetaRecord::Unknown => (),
                }
            }
        }
        Cow::Borrowed(plugin) => {
            for (id, record) in plugin.records.iter() {
                match record {
                    MetaRecord::Record(inner) => {
                        handle_record(
                            Cow::Borrowed(id),
                            Cow::Borrowed(inner),
                            &deleted,
                            &mut new_plugin,
                            &id_map,
                        );
                    }
                    MetaRecord::Delta(_) | MetaRecord::Delete => {
                        new_plugin.records.insert(id.clone(), record.clone());
                    }
                    MetaRecord::Unknown => (),
                }
            }
        }
    }
    new_plugin
}

pub fn parse_file(path: &Path, strip_cells: bool) -> Result<Plugin> {
    // Context is necessary both for loading data from files, and for
    // giving Cell records access to the master list.
    // Initialize thread_local information here.
    crate::plugin::PLUGINS.with(|v| {
        let mut v = v.borrow_mut();
        v.clear();
        v.push(
            path.file_stem()
                .unwrap()
                .to_os_string()
                .into_string()
                .unwrap()
                .to_lowercase(),
        );
    });

    let mut plugin = esplugin::Plugin::new(esplugin::GameId::Morrowind, path);
    plugin
        .parse_file(esplugin::ParseMode::All)
        .context(format!("Failed to parse esm data from {}", path.display()))?;
    let mut out_plugin: Plugin = plugin.try_into().context(format!(
        "Failed to convert plugin {} to DeltaPlugin format",
        path.display().to_string()
    ))?;
    if strip_cells {
        out_plugin.records.retain(|_, v| match v {
            MetaRecord::Record(RecordType::Cell(_)) => false,
            _ => true,
        });
    }
    Ok(out_plugin)
}
