/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use proc_macro2::TokenStream;
use proc_macro_roids::{FieldExt, IdentExt};
use quote::quote;
use syn::{Field, GenericArgument, Ident, Type};

fn get_type_name(field: &Field) -> Option<Ident> {
    if let Type::Path(_) = &field.ty {
        Some(field.type_name().clone())
    } else {
        None
    }
}

fn get_simple_value_type_name(field: &Field) -> Option<Ident> {
    if let syn::Type::Path(syn::TypePath {
        qself: None,
        path: syn::Path {
            leading_colon: None,
            ref segments,
        },
    }) = field.ty
    {
        if let syn::PathSegment {
            ident: _,
            arguments: syn::PathArguments::AngleBracketed(args),
        } = &segments[0]
        {
            if let GenericArgument::Type(syn::Type::Path(syn::TypePath {
                qself: None,
                path:
                    syn::Path {
                        leading_colon: None,
                        segments,
                    },
            })) = &args.args[0]
            {
                return Some(segments[0].ident.clone());
            }
        }
    }
    None
}

/*
fn get_base_path_name(path: &syn::Path) -> Option<String> {
    path.get_ident().map(|x| x.to_string())
}

fn field_has_attr(field: &Field, attr: &str) -> bool {
    field
        .attrs
        .iter()
        .any(|x| get_base_path_name(&x.path) == Some(attr.to_string()))
}
*/

fn is_deletable(typ: &Type) -> bool {
    let (_, inner_type_value) = get_hash_map_key_value_types(typ).unwrap();

    get_outer_type(inner_type_value).map(|x| x.to_string()) == Some("Deletable".to_string())
}

fn is_accumulator(typ: &Type) -> bool {
    let (_, inner_type_value) = get_hash_map_key_value_types(typ).unwrap();

    get_outer_type(inner_type_value).map(|x| x.to_string()) == Some("Accumulator".to_string())
}

fn get_outer_type(typ: &Type) -> Option<&Ident> {
    if let syn::Type::Path(syn::TypePath {
        qself: None,
        path: syn::Path {
            leading_colon: None,
            segments,
        },
    }) = typ
    {
        return Some(&segments[0].ident);
    }
    None
}

fn get_simple_value_type(typ: &Type) -> Option<&Type> {
    if let syn::Type::Path(syn::TypePath {
        qself: None,
        path: syn::Path {
            leading_colon: None,
            segments,
        },
    }) = typ
    {
        if let syn::PathSegment {
            ident: _,
            arguments: syn::PathArguments::AngleBracketed(args),
        } = &segments[0]
        {
            if let GenericArgument::Type(value_type) = &args.args[0] {
                return Some(value_type);
            }
        }
    }
    None
}

/*
fn get_kv_inner_type<'a>(typ: &'a Type, name: &str) -> Option<(&'a Type, &'a Type)> {
    if let syn::Type::Path(syn::TypePath {
        qself: None,
        path: syn::Path {
            leading_colon: None,
            segments,
        },
    }) = typ
    {
        if let syn::PathSegment {
            ident,
            arguments: syn::PathArguments::AngleBracketed(args),
        } = &segments[0]
        {
            if ident == name {
                if let (GenericArgument::Type(key_type), GenericArgument::Type(value_type)) =
                    (&args.args[0], &args.args[1])
                {
                    return Some((key_type, value_type));
                }
            }
        }
    }
    None
}
*/

fn get_hash_map_key_value_types(typ: &Type) -> Option<(&Type, &Type)> {
    if let syn::Type::Path(syn::TypePath {
        qself: None,
        path: syn::Path {
            leading_colon: None,
            segments,
        },
    }) = typ
    {
        if let syn::PathSegment {
            ident,
            arguments: syn::PathArguments::AngleBracketed(args),
        } = &segments[0]
        {
            if *ident == "LinkedHashMap" || *ident == "HashMap" {
                if let (GenericArgument::Type(key_type), GenericArgument::Type(value_type)) =
                    (&args.args[0], &args.args[1])
                {
                    return Some((key_type, value_type));
                }
            }
        }
    }
    None
}

fn should_skip(field: &syn::Field) -> bool {
    field
        .attrs
        .iter()
        .any(|x| x.path.get_ident().is_some() && x.path.get_ident().unwrap() == "skip")
}

fn is_delta(field: &syn::Field) -> bool {
    field
        .attrs
        .iter()
        .any(|x| x.path.get_ident().is_some() && x.path.get_ident().unwrap() == "delta")
}

fn is_multiset(field: &syn::Field) -> bool {
    field
        .attrs
        .iter()
        .any(|x| x.path.get_ident().is_some() && x.path.get_ident().unwrap() == "multiset")
}

#[proc_macro_derive(DeltaRecord, attributes(ignore_missing, delta, skip, multiset))]
pub fn delta_derive(tokens: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = syn::parse(tokens).unwrap();
    let structure = match input.data {
        syn::Data::Struct(structure) => structure,
        _ => panic!("Only structures are supported for DeltaRecords"),
    };

    let record_ident = input.ident;
    let delta_ident = record_ident.prepend("Delta");

    let ignore_missing = input
        .attrs
        .iter()
        .any(|x| x.path.get_ident().is_some() && x.path.get_ident().unwrap() == "ignore_missing");

    let delta_type_body: TokenStream = structure
        .fields
        .iter()
        .map(|field| match &field.ident {
            Some(name) => {
                let typ = &field.ty;
                if should_skip(field) {
                    quote!{}
                } else if is_delta(field) {
                    if get_type_name(field).unwrap() == "Option" {
                        let inner_typ = &get_simple_value_type_name(field).unwrap();
                        let delta_typ = &inner_typ.prepend("Delta");
                        quote! {
                            #name: Option<crate::delta::DeltaUnion<#inner_typ, #delta_typ>>,
                        }
                    } else {
                        let delta_typ = &get_type_name(field).unwrap().prepend("Delta");
                        quote! {
                            #name: Option<#delta_typ>,
                        }
                    }
                } else {
                    match get_type_name(field) {
                        Some(ident) if ident == "Vec" && is_multiset(field) => {
                            let inner_type =
                                get_simple_value_type(typ).unwrap();
                            quote! {
                                #name: Option<Vec<crate::delta::DeletableValue<#inner_type>>>,
                            }

                        }
                        Some(ident) if ident == "LinkedHashSet" => {
                            let inner_type =
                                get_simple_value_type(typ).unwrap();
                            quote! {
                                #name: Option<crate::delta::DeltaSet<#inner_type>>,
                            }
                        }
                        Some(ident) if ident == "LinkedHashMap" => {
                            let (inner_type_key, inner_type_value) =
                                get_hash_map_key_value_types(typ).unwrap();

                            if is_deletable(typ) {
                                let inner_value_type = get_simple_value_type(inner_type_value).unwrap();

                                let delta_inner = if let Type::Path(path) = inner_value_type {
                                    path.path.get_ident().expect("Failed to get type identifier!").prepend("Delta")
                                } else {
                                    panic!("Cannot create Delta for type {:?}", inner_value_type)
                                };

                                quote! {
                                    #name: Option<LinkedHashMap<#inner_type_key, crate::delta::DeltaUnion<#inner_value_type, #delta_inner>>>,
                                }
                            } else {
                                quote! {
                                    #name: Option<LinkedHashMap<#inner_type_key, crate::delta::Deletable<#inner_type_value>>>,
                                }
                            }
                        }
                        _ => {
                            quote! {
                                #name: Option<#typ>,
                            }
                        }
                    }
                }
            }
            None => panic!("Unnamed fields are not supported"),
        })
        .collect();

    let apply_body: TokenStream = structure
        .fields
        .iter()
        .map(|field| {
            match &field.ident {
                Some(name) => {
                    if should_skip(field) {
                        quote!{}
                    } else if is_delta(field) {
                        if get_type_name(field).unwrap() == "Option" {
                            quote! {
                                if let Some(delta) = delta.#name {
                                    match (self.#name.as_mut(), delta) {
                                        (_, crate::delta::DeltaUnion::Add(value)) => self.#name = Some(value),
                                        (Some(ref mut this_value), crate::delta::DeltaUnion::Modify(value)) => this_value.apply_delta(value),
                                        (None, crate::delta::DeltaUnion::Modify(value)) => {
                                            log::error!("Cannot apply delta to nonexistant value! Skipping.");
                                        }
                                        (_, crate::delta::DeltaUnion::Delete) => self.#name = None,
                                    }
                                }
                            }
                        } else {
                            quote! {
                                if let Some(delta) = delta.#name {
                                    self.#name.apply_delta(delta);
                                }
                            }
                        }
                    } else {
                        match get_type_name(field) {
                            Some(ident) if ident == "Vec" && is_multiset(field) => {
                                quote! {
                                    crate::delta::apply_multiset_diff(&mut self.#name, delta.#name);
                                }
                            }
                            Some(ident) if ident == "LinkedHashSet" || ident == "LinkedHashMap" => {
                                match ident.to_string().as_str() {

                                    "LinkedHashSet" => {
                                        quote!{
                                            crate::delta::apply_set_scalar_diff(&mut self.#name, delta.#name, stringify!(#record_ident #name));
                                        }
                                    }
                                    "LinkedHashMap" => {
                                        if is_deletable(&field.ty) {
                                            quote!{
                                                crate::delta::apply_map_delta_diff(&mut self.#name, delta.#name, stringify!(#record_ident #name));
                                            }
                                        } else if is_accumulator(&field.ty) {
                                            quote!{
                                                crate::delta::apply_accumulator(&mut self.#name, delta.#name, stringify!(#record_ident #name));
                                            }
                                        } else {
                                            quote!{
                                                crate::delta::apply_map_scalar_diff(&mut self.#name, delta.#name, stringify!(#record_ident #name));
                                            }
                                        }
                                    }
                                    _ => unreachable!(),
                                }
                            },
                            _ =>
                            quote! {
                                dassign!(self.#name, delta.#name);
                            },
                        }
                    }
                }
                None => panic!("Unnamed fields are not supported"),
            }
        })
        .collect();

    let mut create_header = quote! {};
    let create_body: TokenStream = structure
        .fields
        .iter()
        .map(|field| {
            match &field.ident {
                Some(name) => {
                    if should_skip(field) {
                        quote!{}
                    } else if is_delta(field) {
                        if get_type_name(field).unwrap() == "Option" {
                            let this_name = name.prepend("this_");
                            let other_name = name.prepend("other_");
                            create_header.extend(quote! {
                                let #name = match (self.#name.as_ref(), other.#name.as_ref()) {
                                    (Some(#this_name), Some(#other_name)) => {
                                        if #this_name != #other_name {
                                            Some(crate::delta::DeltaUnion::Modify(#this_name.create_delta(#other_name)))
                                        } else {
                                            None
                                        }
                                    }
                                    (Some(_), None) => Some(crate::delta::DeltaUnion::Delete),
                                    (None, Some(#name)) => Some(crate::delta::DeltaUnion::Add(#name.clone())),
                                    (None, None) => None,
                                };
                            });
                        } else {
                            create_header.extend(quote! {
                                let #name = if self.#name != other.#name {
                                    Some(self.#name.create_delta(&other.#name))
                                } else {
                                    None
                                };
                            });
                        }
                        quote!{
                            #name,
                        }
                    } else {
                        match get_type_name(field) {
                            Some(ident) if ident == "Vec" && is_multiset(field) => {
                                quote!{
                                    #name: crate::delta::create_multiset_diff(&self.#name, &other.#name),
                                }
                            }
                            Some(ident) if ident == "LinkedHashSet" || ident == "LinkedHashMap" => {
                                match ident.to_string().as_str() {
                                    "LinkedHashSet" => {
                                        quote!{
                                            #name: crate::delta::create_set_scalar_diff(&self.#name, &other.#name),
                                        }
                                    }
                                    "LinkedHashMap" => {
                                        if is_deletable(&field.ty) {
                                            quote!{
                                                #name: crate::delta::create_map_delta_diff(&self.#name, &other.#name),
                                            }
                                        } else if is_accumulator(&field.ty) {
                                            quote!{
                                                #name: crate::delta::diff_accumulators(&self.#name, &other.#name),
                                            }
                                        } else {
                                            quote!{
                                                #name: crate::delta::create_map_scalar_diff(&self.#name, &other.#name),
                                            }
                                        }
                                    }
                                    _ => unreachable!(),
                                }
                            },
                            // For records marked as ignore_missing (e.g. those where the engine
                            // loads new records on top of the old ones, such as Cell Records)
                            // Report missing records as None, rather than Some(None) (i.e.
                            // deleted)
                            Some(ident) if ident == "Option" && ignore_missing => quote!{
                                #name: ddiff_opt!(self.#name, other.#name),
                            },
                            _ => quote! {
                                #name: ddiff!(self.#name, other.#name),
                            },
                        }
                    }
                }
                None => panic!("Unnamed fields are not supported"),
            }
        })
        .collect();

    let generics = input.generics;

    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let empty_body: TokenStream = structure
        .fields
        .iter()
        .map(|field| {
            let name = &field.ident;
            quote! {
                if self.#name.is_some() {
                    return false;
                }
            }
        })
        .collect();

    let result = quote! {
        #[skip_serializing_none]
        #[derive(Clone, Debug, Default, Serialize, Deserialize)]
        pub struct #delta_ident#generics {
            #delta_type_body
        }

        impl #impl_generics crate::delta::Delta for #delta_ident#ty_generics #where_clause {
            fn is_empty(&self) -> bool {
                #empty_body
                true
            }
        }

        impl #impl_generics crate::delta::DeltaOf<#delta_ident #ty_generics> for #record_ident #ty_generics #where_clause {

            fn apply_delta(&mut self, delta: #delta_ident #ty_generics) {
                #apply_body
            }

            fn create_delta(&self, other: &Self) -> #delta_ident #ty_generics {
                #create_header
                #delta_ident {
                    #create_body
                }
            }
        }
    };
    result.into()
}

#[proc_macro_derive(OverrideRecord)]
pub fn override_derive(tokens: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = syn::parse(tokens).unwrap();
    let structure = match input.data {
        syn::Data::Struct(structure) => structure,
        _ => panic!("Only structures are supported for Records"),
    };

    let apply_body: TokenStream = structure
        .fields
        .iter()
        .map(|field| match &field.ident {
            Some(name) => {
                quote! {
                    self.#name = other.#name;
                }
            }
            None => panic!("Unnamed fields are not supported"),
        })
        .collect();

    let ident = input.ident;

    let result = quote! {
        impl crate::record::FullRecord for #ident {
            fn get_patch(&self, master: &crate::record::RecordType) -> Option<crate::record::MetaRecord> {
                use crate::record::{MetaRecord, RecordType};
                if let RecordType::#ident(master) = master {
                    if master != self {
                        Some(MetaRecord::Record(RecordType::#ident(self.clone())))
                    } else {
                        None
                    }
                } else {
                    panic!("Mismatched record types for records {:?} and {:?}!", self, master);
                }
            }

            fn apply_patch(&mut self, other: crate::record::MetaRecord) {
                use crate::record::{MetaRecord, RecordType};
                if let MetaRecord::Record(RecordType::#ident(other)) = other {
                    #apply_body
                }
            }
        }
    };
    result.into()
}

#[proc_macro_derive(FullRecord)]
pub fn full_record_derive(tokens: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = syn::parse(tokens).unwrap();

    let ident = input.ident;
    let delta_ident = ident.prepend("Delta");

    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

    let result = quote! {
        impl #impl_generics crate::record::FullRecord for #ident #ty_generics #where_clause {
            fn get_patch(&self, master: &crate::record::RecordType) -> Option<crate::record::MetaRecord> {
                use crate::record::{MetaRecord, RecordType, DeltaRecordType};
                use crate::delta::DeltaOf;
                if let RecordType::#ident(master) = master {
                    if master != self {
                        Some(MetaRecord::Delta(DeltaRecordType::#delta_ident(master.create_delta(self))))
                    } else {
                        None
                    }
                } else {
                    panic!("Mismatched record types for records {:?} and {:?}!", self, master);
                }
            }

            fn apply_patch(&mut self, other: crate::record::MetaRecord) {
                use crate::record::{MetaRecord, DeltaRecordType};
                use crate::delta::DeltaOf;
                if let MetaRecord::Delta(DeltaRecordType::#delta_ident(other)) = other {
                    self.apply_delta(other);
                }
            }
        }
    };
    result.into()
}
